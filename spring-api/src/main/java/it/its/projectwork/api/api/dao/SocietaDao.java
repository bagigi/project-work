package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "societa")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SocietaDao {
    @Id
    @Column(name = "societaId")
    public int id;

    @Column(name = "ragioneSociale")
    public String ragione_sociale;

    @Column(name = "indirizzo")
    public String indirizzo;

    @Column(name = "provincia")
    public String provincia;

    @Column(name = "nazione")
    public String nazione;

    @Column(name = "telefono")
    public String telefono;

    @Column(name = "fax")
    public String fax;

    @Column(name = "partitaIva")
    public String partita_iva;
}
