package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.controller.AlunniController;
import it.its.projectwork.api.api.dao.CorsiAlunniDao;
import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dto.CorsiAlunniDto;
import it.its.projectwork.api.api.dto.DipendentiDto;
import it.its.projectwork.api.api.repository.*;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CorsiAlunniServiceImp implements CorsiAlunniService{
    @Autowired
    CorsiAlunniRepository corsiAlunniRepository;
    @Autowired
    DocentiRepository docentiRepository;
    @Autowired
    DipendentiService dipendentiService;
    @Autowired
    CorsiDocentiRepository corsiDocentiRepository;

    @Override
    public int[] getPages() {
        int items = this.corsiAlunniRepository.getPages();
        int pages = items/10;
        if(items%10 != 0) pages++;

        int[] kk = new int[pages];
        for(int i = 0; i != pages; i++){
            kk[i] = i+1;
        }

        return kk;
    }

    @Override
    public List<CorsiAlunniDao> getByPage(int page) {
        page = (page - 1)*10;

        List<CorsiAlunniDao> dao = this.corsiAlunniRepository.getByPage(page, page + 10);

        return dao;
    }

    @Override
    public CorsiAlunniDao insertAlunno(int corso, int alunno) {
        CorsiAlunniDao dao = new CorsiAlunniDao();
        dao.setCorso(corso);
        dao.setDipendente(alunno);
        dao.setDurata(-1);

        this.corsiAlunniRepository.saveAndFlush(dao);

        return null;
    }

    public CorsiAlunniDao insertMultipleAlunni(int corso, int[] alunni){
        List<CorsiAlunniDao> dao = new ArrayList<>();
        Integer lastId = this.corsiAlunniRepository.getLastId();
        if(lastId == null) lastId = 0;

        for(int i = 0; i != alunni.length; i++){
            CorsiAlunniDao d = new CorsiAlunniDao();
            lastId++;
            d.setId(lastId);
            d.setCorso(corso);
            d.setDipendente(alunni[i]);
            d.setDurata(-1);

            dao.add(d);
        }

        System.out.println("dio boia: " + Arrays.toString(dao.toArray()));

        this.corsiAlunniRepository.saveAll(dao);

        return null;
    }

    @Override
    public ArrayList<CorsiAlunniDto> getByCorsoId(int idCorso) {
        CorsiAlunniDao[] alunni = this.corsiAlunniRepository.getByCorsoId(idCorso);
        ArrayList<CorsiAlunniDto> dto = new ArrayList<>();

        for(CorsiAlunniDao d : alunni){
            CorsiAlunniDto temp = new CorsiAlunniDto();

            temp.setCorso(d.getCorso());
            temp.setId(d.getId());
            temp.setDurata(d.getDurata());

            DipendentiDto dipendente = this.dipendentiService.getById(d.getDipendente());

            temp.setDipendente(dipendente);

            dto.add(temp);
        }

        return dto;
    }

    @Override
    public void deleteRow(int idDipendente, int idCorso) {
        this.corsiAlunniRepository.deleteRow(idDipendente, idCorso);
        return;
    }
}
