package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tipi")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TipiDao {
    @Id
    @Column(name = "tipoId")
    private int tipo;

    @Column(name = "descrizione")
    private String descrizione;
}
