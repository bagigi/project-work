package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "misure")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MisureDao {
    @Id
    @Column(name = "misuraId")
    private int misura;

    @Column(name = "descrizione")
    private String descrizione;
}
