package it.its.projectwork.api.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;

@Getter
@Setter
public class DipendentiDto {
    @Column(name = "dipendenteId")
    private int id;

    @Column(name = "matricola")
    private int matricola;

    @Column(name = "societa")
    private int societa;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "nome")
    private String nome;

    @Column(name = "sesso")
    private String sesso;

    @Column(name = "tipoDipendente")
    private String tipo_dipendente;

    @Column(name = "qualifica")
    private String qualifica;

    @Column(name = "livello")
    private String livello;
}
