package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.CorsiDocentiDao;
import it.its.projectwork.api.api.dao.ValutazioniCorsiDao;
import it.its.projectwork.api.api.dao.ValutazioniDocentiDao;
import it.its.projectwork.api.api.repository.CorsiDocentiRepository;
import it.its.projectwork.api.api.repository.ValutazioniDocentiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ValutazioniDocentiServiceImp implements ValutazioniDocentiService{
    @Autowired
    ValutazioniDocentiRepository valutazioniDocentiRepository;
    @Autowired
    DocentiServiceImp docentiServiceImp;
    @Autowired
    CorsiDocentiRepository corsiDocentiRepository;

    @Override
    public void inserisciValutazione(ValutazioniDocentiDao dao) {
        this.valutazioniDocentiRepository.saveAndFlush(dao);
    }

    @Override
    public ValutazioniDocentiDao getByDocenteAndCorso(int docente, int corso) {
        List<ValutazioniDocentiDao> dao = this.valutazioniDocentiRepository.findAll();
        ValutazioniDocentiDao d = new ValutazioniDocentiDao();

        for(ValutazioniDocentiDao lel : dao){
            if(lel.getCorso() == corso && lel.getDocente() == docente) d = lel;
        }

        return d;
    }

    @Override
    public ArrayList<ValutazioniDocentiDao> getValutazioniDocentiInterni(int docenteId, int page) {
        page = (page - 1) * 10;
        List<ValutazioniDocentiDao> dao = this.valutazioniDocentiRepository.getValutazioniByDocente(docenteId, page, page +10);
        ArrayList<ValutazioniDocentiDao> lista = new ArrayList<>();

        int[] corsi = this.corsiDocentiRepository.getCorsiByDocenteInterno(docenteId);
        int[] docentiInterni = this.docentiServiceImp.getAllDocentiInterni();

        System.out.println("CORSI: " + Arrays.toString(corsi));
        System.out.println("DOCENTI INTERNI: " + Arrays.toString(docentiInterni));

        for(ValutazioniDocentiDao d: dao) {
            System.out.println("VALUTAZIONI: " + d.getCorso());
            if(Arrays.stream(corsi).anyMatch(i -> i == d.getCorso())){
                if(Arrays.stream(docentiInterni).anyMatch(i -> i == d.getDocente())){
                    lista.add(d);
                }
            }
        }

        return lista;
    }

    @Override
    public ArrayList<ValutazioniDocentiDao> getValutazioniDocentiEsterni(int docenteId, int page) {
        page = (page - 1) * 10;
        List<ValutazioniDocentiDao> dao = this.valutazioniDocentiRepository.getValutazioniByDocente(docenteId, page, 10);
        ArrayList<ValutazioniDocentiDao> lista = new ArrayList<>();

        int[] corsi = this.corsiDocentiRepository.getCorsiByDocenteEsterno(docenteId);
        int[] docentiInterni = this.docentiServiceImp.getAllDocentiInterni();

        System.out.println("CORSI: " + Arrays.toString(corsi));
        System.out.println("DOCENTI INTERNI: " + Arrays.toString(docentiInterni));

        for(ValutazioniDocentiDao d: dao) {
            System.out.println("VALUTAZIONI: " + d.getCorso());
            if(Arrays.stream(corsi).anyMatch(i -> i == d.getCorso())){
                if(Arrays.stream(docentiInterni).anyMatch(i -> i == d.getDocente())){
                    lista.add(d);
                }
            }
        }

        return lista;
    }

    /**
     * Paginazione
     */
    @Override
    public int[] getPagesDocentiAll() {
        Integer items = this.valutazioniDocentiRepository.getPagesDocentiAll();

        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    @Override
    public int[] getPagesByDocenteId(int docenteId, int tipoDocente) {
        Integer items = this.valutazioniDocentiRepository.getPagesByDocenteId(docenteId, tipoDocente);
        System.out.println("dioo: " + items);
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    /**
     * Eliminazione
     */
    @Override
    public boolean elimina(int id) {
        this.valutazioniDocentiRepository.deleteById(id);
        if(this.valutazioniDocentiRepository.findById(id).isPresent()) return false;
        else return true;
    }
}
