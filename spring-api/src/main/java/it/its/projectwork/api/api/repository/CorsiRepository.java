package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.CorsiDao;
import it.its.projectwork.api.api.dto.CorsiDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface CorsiRepository extends JpaRepository<CorsiDao, Integer> {
    ArrayList<CorsiDao> findByTipologia(int tipologia);

    @Query(nativeQuery = true, value = "select count(*) from corsi")
    Integer getCorsiPages();

    @Query(nativeQuery = true, value = "select * from corsi limit ?1, ?2")
    ArrayList<CorsiDao> getCorsiByPage(int start, int stop);

    @Query(nativeQuery = true, value = "SELECT * FROM corsi ORDER BY corso_id DESC LIMIT 1")
    CorsiDao getUltimoCorso();

    /**
     * Get By Alunno con paginazione
     */
    @Query(nativeQuery = true, value = "select count(*) from corsi_utenti" +
            " inner join corsi on corsi.corso_id = corsi_utenti.corso" +
            " where corsi_utenti.dipendente = ?1" +
            " and corsi.data_chiusura >= curdate()")
    Integer getAlunnoPages(int idAlunno);
    @Query(nativeQuery = true, value = "select * from corsi" +
            " inner join corsi_utenti on corsi.corso_id = corsi_utenti.corso" +
            " where corsi_utenti.dipendente = ?1" +
            " and corsi.data_chiusura >= curdate()" +
            " limit ?2, ?3")
    List<CorsiDao> getCorsoByAlunno(int id, int start, int stop);
}
