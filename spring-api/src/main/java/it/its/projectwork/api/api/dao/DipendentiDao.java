package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "dipendenti")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DipendentiDao {
    @Id
    @Column(name = "dipendenteId")
    private int dipendente_id;

    @Column(name = "matricola")
    private int matricola;

    @Column(name = "societa")
    private int societa;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "nome")
    private String nome;

    @Column(name = "sesso")
    private String sesso;

    @Column(name = "dataNascita")
    private Date data_nascita;

    @Column(name = "luogoNascita")
    private String luogo_nascita;

    @Column(name = "statoCivile")
    private String stato_civile;

    @Column(name = "titoloStudio")
    private String titolo_studio;

    @Column(name = "conseguitoPresso")
    private String conseguito_presso;

    @Column(name = "annoConseguimento")
    private int anno_conseguimento;

    @Column(name = "tipoDipendente")
    private String tipo_dipendente;

    @Column(name = "qualifica")
    private String qualifica;

    @Column(name = "livello")
    private String livello;

    @Column(name = "dataAssunzione")
    private Date data_assunzione;

    @Column(name = "responsabileRisorsa")
    private String responsabile_risorsa;

    @Column(name = "responsabileArea")
    private String responsabile_area;

    @Column(name = "dataFineRapporto")
    private Date data_fine_rapporto;

    @Column(name = "dataScadenzaContratto")
    private Date data_scadenza_contratto;
}
