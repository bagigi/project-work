package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dao.TipiDao;
import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.repository.TipiRepository;
import it.its.projectwork.api.api.service.TipiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "api/tipi")
public class TipiController {
    @Autowired
    TipiService tipiService;

    @GetMapping(produces = "application/json")
    public BaseResponseDto<List<TipiDao>> getAll(){
        BaseResponseDto<List<TipiDao>> responseDto = new BaseResponseDto<>();

        List<TipiDao> dao = this.tipiService.getAll();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dao);

        return responseDto;
    }
}
