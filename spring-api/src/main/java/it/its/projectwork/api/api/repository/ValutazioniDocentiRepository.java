package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.ValutazioniDocentiDao;
import it.its.projectwork.api.api.dao.ValutazioniUtentiDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ValutazioniDocentiRepository extends JpaRepository<ValutazioniDocentiDao, Integer> {
    @Query(nativeQuery = true, value = "select * from valutazioni_docenti where docente = ?1 limit ?2, ?3")
    List<ValutazioniDocentiDao> getValutazioniByDocente(int docenteId, int start, int stop);

    @Query(nativeQuery = true, value = "select * from valutazioni_docenti where docente = ?1 and corso = ?1")
    ValutazioniDocentiDao getValutazioneByDocenteAndCorso(int docente, int corso);

    /**
     * Paginazione
     */
    @Query(nativeQuery = true, value = "select count(*) from valutazioni_docenti")
    Integer getPagesDocentiAll();
    @Query(nativeQuery = true, value = "select count(*) from valutazioni_docenti" +
            " inner join corsi_docenti on valutazioni_docenti.docente = corsi_docenti.docente" +
            " where valutazioni_docenti.docente = ?1" +
            " and corsi_docenti.interno = ?2")
    Integer getPagesByDocenteId(int docenteId, int tipoDocente);

    void deleteByCorso(int idCorso);

    @Query(nativeQuery = true, value = "select * from valutazioni_docenti where corso = ?1")
    List<ValutazioniDocentiDao> getByCorso(int corsoId);
}
