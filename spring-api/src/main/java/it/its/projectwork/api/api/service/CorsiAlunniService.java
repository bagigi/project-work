package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.CorsiAlunniDao;
import it.its.projectwork.api.api.dto.CorsiAlunniDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface CorsiAlunniService {
    public int[] getPages();
    public List<CorsiAlunniDao> getByPage(int page);
    public CorsiAlunniDao insertAlunno(int corso, int alunno);
    public CorsiAlunniDao insertMultipleAlunni(int corso, int[] alunni);
    public ArrayList<CorsiAlunniDto> getByCorsoId(int idCorso);
    public void deleteRow(int idDipendente, int idCorso);
}
