package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.ValutazioniCorsiDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ValutazioniCorsiRepository extends JpaRepository<ValutazioniCorsiDao, Integer> {
    @Query(nativeQuery = true, value = "select * from valutazioni_corsi where corso = ?1")
    Optional<ValutazioniCorsiDao> getByCorsoId(int corsoId);

    /**
     * Paginazione
     */
    @Query(nativeQuery = true, value = "select count(*) from valutazioni_corsi")
    Integer getPagesCorsoAll();

    void deleteByCorso(int idCorso);
}
