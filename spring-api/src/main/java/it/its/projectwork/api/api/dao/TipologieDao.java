package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipologie")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TipologieDao {
    @Id
    @Column(name = "tipologiaId")
    private Integer tipologia;

    @Column(name = "descrizione")
    private String descrizione;
}
