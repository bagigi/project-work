package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "corsi_utenti")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CorsiAlunniDao {
    @Id
    @Column(name = "CorsiUtentiId")
    public int id;

    @Column(name = "corso")
    public int corso;

    @Column(name = "dipendente")
    public int dipendente;

    @Column(name = "durata")
    public int durata;
}
