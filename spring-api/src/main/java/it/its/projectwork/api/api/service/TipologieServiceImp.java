package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.TipologieDao;
import it.its.projectwork.api.api.repository.TipologieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TipologieServiceImp implements TipologieService{
    @Autowired
    TipologieRepository tipologieRepository;

    @Override
    public List<TipologieDao> getAll() {
        List<TipologieDao> dao = this.tipologieRepository.findAll();
        return dao;
    }
}
