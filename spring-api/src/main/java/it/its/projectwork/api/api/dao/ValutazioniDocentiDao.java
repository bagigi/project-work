package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "valutazioni_docenti")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ValutazioniDocentiDao {
    @Id
    @Column(name = "valutazioneDocentiId")
    public int id;

    @Column(name = "completezza")
    public int completezza;

    @Column(name = "chiarezza")
    public int chiarezza;

    @Column(name = "disponibilita")
    public int disponibilita;

    @Column(name = "media")
    public float media;

    @Column(name = "docente")
    public int docente;

    @Column(name = "corso")
    public int corso;
}
