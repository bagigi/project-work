package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dao.CorsiDocentiDao;
import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dao.DocentiDao;
import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.dto.DipendentiDto;
import it.its.projectwork.api.api.dto.DocentiDto;
import it.its.projectwork.api.api.repository.CorsiDocentiRepository;
import it.its.projectwork.api.api.service.DocentiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping(value = "api/docenti")
public class DocentiController {
    @Autowired
    DocentiService docentiService;
    @Autowired
    CorsiDocentiRepository corsiDocentiRepository;

    @GetMapping(produces = "application/json")
    public BaseResponseDto<ArrayList<DocentiDto>> getAll() {
        BaseResponseDto<ArrayList<DocentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DocentiDto> dto = docentiService.getAll();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public BaseResponseDto<DocentiDto> getById(@PathVariable("id") int id) {
        BaseResponseDto<DocentiDto> responseDto = new BaseResponseDto<>();

        DocentiDto dto = this.docentiService.getById(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @PostMapping(produces = "application/json")
    public BaseResponseDto<DocentiDto> create(@RequestBody DocentiDao docente) {
        BaseResponseDto<DocentiDto> responseDto = new BaseResponseDto<>();

        System.out.println("UTENTE IN ENTRATA: " + docente.toString());

        DocentiDto dto = this.docentiService.create(docente);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public BaseResponseDto<DocentiDto> deleteById(@PathVariable("id") int id) {
        BaseResponseDto<DocentiDto> responseDto = new BaseResponseDto<>();

        boolean resp = this.docentiService.delete(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(resp);

        return responseDto;
    }

    @PatchMapping(produces = "application/json", consumes = "application/json", value = "/{id}")
    public BaseResponseDto<DocentiDao> update(@RequestBody DocentiDto docente, @PathVariable("id") int id) {
        System.out.println("EDIT DOCENTE: ");
        System.out.println("OBJ: " + docente.getRagione_sociale());

        BaseResponseDto<DocentiDao> responseDao = new BaseResponseDto<>();

        DocentiDao d = this.docentiService.update(docente, id);

        responseDao.setTimestamp(new Date());
        responseDao.setStatus(HttpStatus.OK.value());
        responseDao.setMessage("Servizio elaborato correttamente");
        responseDao.setResponse(d);

        return responseDao;
    }

    @GetMapping(value = "/page/{page}", produces = "application/json")
    public BaseResponseDto<ArrayList<DocentiDto>> getByPage(@PathVariable("page") int page) {
        BaseResponseDto<ArrayList<DocentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DocentiDto> dto = this.docentiService.getByPage(page);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @GetMapping(produces = "application/json", value = "/info/pages/all")
    public BaseResponseDto<DocentiDto> getInfoPagesAll() {
        BaseResponseDto<DocentiDto> responseDto = new BaseResponseDto<>();

        int items = this.docentiService.getDocentiRecord();
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(kk);

        return responseDto;
    }

    @GetMapping(produces = "application/json", value = "/info/pages/filter")
    public BaseResponseDto<Integer> getInfoPagesFilter(@RequestParam String filter, @RequestParam String value) {
        BaseResponseDto<Integer> responseDto = new BaseResponseDto<>();

        int items = this.docentiService.getDocentiFilterRecord(filter, value);
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(kk);

        return responseDto;
    }

    @GetMapping(produces = "application/json", value = "/page/filter")
    public BaseResponseDto<DocentiDto> getInfoPagesFilter(@RequestParam String filter, @RequestParam String value,
            @RequestParam int page) {
        BaseResponseDto<DocentiDto> responseDto = new BaseResponseDto<>();

        ArrayList<DocentiDto> dto = this.docentiService.getFilterByPage(filter, value, page);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @GetMapping(produces = "application/json", value = "/filter/{value}")
    public BaseResponseDto<ArrayList<DocentiDto>> getByRagioneSociale(@PathVariable("value") String value) {
        BaseResponseDto<ArrayList<DocentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DocentiDto> dto = this.docentiService.getByRagioneSociale(value);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    /**
     * Paginazione docente interno
     * @return
     */

    @GetMapping(value = "/info/interno/pages/all", produces = "application/json")
    public BaseResponseDto<DipendentiDao> getMethodName() {
        BaseResponseDto<DipendentiDao> responseDto = new BaseResponseDto<>();

        int[] pages = this.docentiService.getDocentiInterniPages();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(pages);

        return responseDto;
    }

    @GetMapping(value = "/interno/page/{page}")
    public BaseResponseDto<ArrayList<DipendentiDto>> getMethodName(@PathVariable("page") int page) {
        BaseResponseDto<ArrayList<DipendentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DipendentiDto> dto = this.docentiService.getDocentiInterni(page);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    /**
     * Filtri docente interno
     */
    @GetMapping(value = "/info/interno/pages/filtro")
    public BaseResponseDto<int[]> getPageByFilter(@RequestParam String filtro, @RequestParam String valore){
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        if(filtro.equals("nome")) responseDto.setResponse(this.docentiService.getDocentiInterniPagesFilterNome(valore));
        else if(filtro.equals("cognome")) responseDto.setResponse(this.docentiService.getDocentiInterniPagesFilterCognome(valore));

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }

    @GetMapping(value = "/interno/filtro", produces = "application/json")
    public BaseResponseDto<ArrayList<DipendentiDto>> filterInterno(
            @RequestParam String filtro,
            @RequestParam String valore,
            @RequestParam int page
    ){
        BaseResponseDto<ArrayList<DipendentiDto>> responseDto = new BaseResponseDto<>();

        if(filtro.equals("nome")) responseDto.setResponse(this.docentiService.filterDocentiInterniByNome(valore, page));
        else if(filtro.equals("cognome")) responseDto.setResponse(this.docentiService.filterDocentiInterniByCognome(valore, page));

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }

    @GetMapping(value = "/info/relazioni/{tipo}/{id}", produces = "application/json")
    BaseResponseDto<Boolean> getRelazioni(
            @PathVariable("tipo") int tipo,
            @PathVariable("id") int id
    ) {
        BaseResponseDto<Boolean> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.docentiService.getRelazioni(tipo, id));

        return responseDto;
    }
}