package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.CorsiDocentiDao;
import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dao.DocentiDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface DocentiRepository extends JpaRepository<DocentiDao, Integer> {
    /**
     * ritorna tutta la lista
     * 
     * @return
     */
    @Query(nativeQuery = true, value = "select count(*) from docenti")
    Integer getDocentiCount();

    @Query(nativeQuery = true, value = "select * from docenti limit ?1, ?2")
    List<DocentiDao> findByPage(int page, int limit);

    /**
     * filtro per nome
     * 
     * @param value
     * @return
     */
    @Query(nativeQuery = true, value = "select count(*) from docenti where nome LIKE %?1%")
    Integer getNomeCount(String value);

    @Query(nativeQuery = true, value = "select * from docenti where nome LIKE %?1% limit ?2, ?3")
    List<DocentiDao> filterByNome(String value, int page, int limit);

    /**
     * filtro per cognome
     */
    @Query(nativeQuery = true, value = "select count(*) from docenti where cognome like %?1%")
    Integer getCognomeCount(String value);

    @Query(nativeQuery = true, value = "select * from docenti where cognome LIKE %?1% limit ?2, ?3")
    List<DocentiDao> filterByCognome(String value, int page, int limit);

    /**
     * filtro per ragione sociale
     */
    @Query(nativeQuery = true, value = "select count(*) from docenti where ragione_sociale like %?1%")
    Integer getRagioneSocialeCount(String value);

    @Query(nativeQuery = true, value = "select * from docenti where ragione_sociale LIKE %?1% limit ?2, ?3")
    List<DocentiDao> filterByRagioneSociale(String value, int page, int limit);

    @Query(nativeQuery = true, value = "select * from docenti where ragione_sociale LIKE %?1%")
    List<DocentiDao> filterByRagioneSocialeNoLimit(String value);

    /**
     * filtro per titolo
     */
    @Query(nativeQuery = true, value = "select count(*) from docenti where titolo like %?1%")
    Integer getTitoloCount(String value);

    @Query(nativeQuery = true, value = "select * from docenti where titolo LIKE %?1% limit ?2, ?3")
    List<DocentiDao> filterByTitolo(String value, int page, int limit);


}
