package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "valutazione_utenti")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ValutazioniUtentiDao {
    @Id
    @Column(name = "valutazioneUtentiId")
    public int id;

    @Column(name = "logica")
    public int logica;

    @Column(name = "apprendimento")
    public int apprendimento;

    @Column(name = "velocita")
    public int velocita;

    @Column(name = "impegno")
    public int impegno;

    @Column(name = "ordinePrecisione")
    public int ordine_precisione;

    @Column(name = "responsabile")
    public int responsabile;

    @Column(name = "media")
    public float media;

    @Column(name = "dipendente")
    public int dipendente;

    @Column(name = "corso")
    public int corso;
}
