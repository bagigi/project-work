package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.CorsiDocentiDao;
import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dao.DocentiDao;
import it.its.projectwork.api.api.dto.DipendentiDto;
import it.its.projectwork.api.api.dto.DocentiDto;
import it.its.projectwork.api.api.repository.CorsiDocentiRepository;
import it.its.projectwork.api.api.repository.DipendentiRepository;
import it.its.projectwork.api.api.repository.DocentiRepository;
import it.its.projectwork.api.api.repository.ValutazioniDocentiRepository;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.swing.text.html.Option;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@Service
@Transactional
public class DocentiServiceImp implements DocentiService {

    @Autowired
    DocentiRepository docentiRepository;
    @Autowired
    DipendentiService dipendentiService;
    @Autowired
    DipendentiRepository dipendentiRepository;
    @Autowired
    CorsiDocentiRepository corsiDocentiRepository;
    @Autowired
    ValutazioniDocentiRepository valutazioniDocentiRepository;

    @Override
    public ArrayList<DocentiDto> getAll() {
        List<DocentiDao> dao = this.docentiRepository.findAll();
        ArrayList<DocentiDto> dto = new ArrayList<>();

        for (DocentiDao d : dao) {
            DocentiDto temp = new DocentiDto();
            temp.setTitolo(d.getTitolo());
            temp.setRagione_sociale(d.getRagioneSociale());
            temp.setNome(d.getNome());
            temp.setCognome(d.getCognome());
            temp.setId(d.getDocentiId());
            temp.setFax(d.getFax());
            temp.setPartita_iva(d.getPartita_iva());
            temp.setReferente(d.getReferente());
            temp.setTelefono(d.getTelefono());

            dto.add(temp);
        }

        return dto;
    }

    @Override
    public DocentiDto getById(int id) {
        Optional<DocentiDao> dao = this.docentiRepository.findById(id);

        if (dao.isPresent()) {
            DocentiDto dto = new DocentiDto();
            DocentiDao d = dao.get();
            dto.setTitolo(d.getTitolo());
            dto.setRagione_sociale(d.getRagioneSociale());
            dto.setNome(d.getNome());
            dto.setCognome(d.getCognome());
            dto.setId(d.getDocentiId());
            dto.setFax(d.getFax());
            dto.setPartita_iva(d.getPartita_iva());
            dto.setReferente(d.getReferente());
            dto.setTelefono(d.getTelefono());

            return dto;
        }

        return null;
    }

    @Override
    public DocentiDto create(DocentiDao docente) {
        this.docentiRepository.saveAndFlush(docente);
        return null;
    }

    @Override
    public boolean delete(int id) {
        Optional<DocentiDao> dao = this.docentiRepository.findById(id);

        if (dao.isPresent()) {
            this.docentiRepository.delete(dao.get());
            return true;
        }

        int[] corso = this.corsiDocentiRepository.getCorsiByDocenteEsterno(id);
        this.corsiDocentiRepository.deleteByDocenteId(id);
        for(int i = 0; i <= corso.length; i++){
            this.valutazioniDocentiRepository.deleteByCorso(corso[i]);
        }

        return false;
    }

    @Override
    public DocentiDao update(DocentiDto docente, int id) {
        Optional<DocentiDao> opt = this.docentiRepository.findById(id);

        if (opt.isPresent()) {
            DocentiDao dao = opt.get();

            dao.setCognome(docente.getCognome());
            dao.setNome(docente.getNome());
            dao.setTitolo(docente.getTitolo());
            dao.setRagioneSociale(docente.getRagione_sociale());
            dao.setIndirizzo(opt.get().getIndirizzo());
            dao.setLocalita(opt.get().getLocalita());
            dao.setProvincia(opt.get().getProvincia());
            dao.setNazione(opt.get().getNazione());
            dao.setTelefono(docente.getTelefono());
            dao.setFax(docente.getFax());
            dao.setPartita_iva(docente.getPartita_iva());
            dao.setReferente(docente.getReferente());

            System.out.println("prima db: " + dao.toString());

            DocentiDao d = this.docentiRepository.saveAndFlush(dao);
            return d;
        }

        return null;
    }

    @Override
    public ArrayList<DocentiDto> getByPage(int page) {
        page = (page - 1) * 10;
        List<DocentiDao> dao = this.docentiRepository.findByPage(page, 10);

        ArrayList<DocentiDto> dto = new ArrayList<>();

        for (DocentiDao d : dao) {
            DocentiDto temp = new DocentiDto();
            temp.setTitolo(d.getTitolo());
            temp.setRagione_sociale(d.getRagioneSociale());
            temp.setNome(d.getNome());
            temp.setCognome(d.getCognome());
            temp.setId(d.getDocentiId());
            temp.setFax(d.getFax());
            temp.setPartita_iva(d.getPartita_iva());
            temp.setReferente(d.getReferente());
            temp.setTelefono(d.getTelefono());
            dto.add(temp);
        }

        return dto;
    }

    @Override
    public int getDocentiRecord() {
        return this.docentiRepository.getDocentiCount();
    }

    @Override
    public int getDocentiFilterRecord(String filter, String value) {
        switch (filter) {
            case "nome":
                return this.docentiRepository.getNomeCount(value);
            case "cognome":
                return this.docentiRepository.getCognomeCount(value);
            case "titolo":
                return this.docentiRepository.getTitoloCount(value);
            case "ragione_sociale":
                return this.docentiRepository.getRagioneSocialeCount(value);
        }
        return 0;
    }

    @Override
    public ArrayList<DocentiDto> getFilterByPage(String filter, String value, int page) {
        page = (page - 1) * 10;

        List<DocentiDao> dao = new ArrayList<>();

        ArrayList<DocentiDto> dto = new ArrayList<>();

        switch (filter) {
            case "nome":
                dao = this.docentiRepository.filterByNome(value, page, 10);
                break;
            case "cognome":
                dao = this.docentiRepository.filterByCognome(value, page, 10);
                break;
            case "titolo":
                dao = this.docentiRepository.filterByTitolo(value, page, 10);
                break;
            case "ragione_sociale":
                dao = this.docentiRepository.filterByRagioneSociale(value, page, 10);
        }

        for (DocentiDao d : dao) {
            DocentiDto temp = new DocentiDto();
            temp.setTitolo(d.getTitolo());
            temp.setRagione_sociale(d.getRagioneSociale());
            temp.setNome(d.getNome());
            temp.setCognome(d.getCognome());
            temp.setId(d.getDocentiId());
            temp.setFax(d.getFax());
            temp.setPartita_iva(d.getPartita_iva());
            temp.setReferente(d.getReferente());
            temp.setTelefono(d.getTelefono());
            dto.add(temp);
        }

        return dto;
    }

    public ArrayList<DocentiDto> getByRagioneSociale(String value) {
        List<DocentiDao> dao = this.docentiRepository.filterByRagioneSocialeNoLimit(value);
        ArrayList<DocentiDto> dto = new ArrayList<>();

        for (DocentiDao d : dao) {
            DocentiDto temp = new DocentiDto();
            temp.setTitolo(d.getTitolo());
            temp.setRagione_sociale(d.getRagioneSociale());
            temp.setNome(d.getNome());
            temp.setCognome(d.getCognome());
            temp.setId(d.getDocentiId());
            temp.setFax(d.getFax());
            temp.setPartita_iva(d.getPartita_iva());
            temp.setReferente(d.getReferente());
            temp.setTelefono(d.getTelefono());
            dto.add(temp);
        }

        return dto;
    }


    /**
     * Docenti interni paginazione
     * @return
     */

    public int[] getAllDocentiInterni(){
        List<CorsiDocentiDao> corsiDao = this.corsiDocentiRepository.findAll();
        ArrayList<DipendentiDto> dto = new ArrayList<>();
        int[] ids = new int[corsiDao.size()];

        int k = 0;
        for(CorsiDocentiDao d : corsiDao){
            if(!Arrays.stream(ids).anyMatch(i -> i == d.getDocente())){
                if(d.getInterno() == 0) {
                    DipendentiDto temp = new DipendentiDto();
                    temp = this.dipendentiService.getById(d.getDocente());
                    dto.add(temp);
                    ids[k] = d.getDocente();
                    k++;
                }
            }
        }

        return ids;
    }

    public int[] getAllDocentiEsterni() {
        List<CorsiDocentiDao> corsiDao = this.corsiDocentiRepository.findAll();
        ArrayList<DipendentiDto> dto = new ArrayList<>();
        int[] ids = new int[corsiDao.size()];

        int k = 0;
        for(CorsiDocentiDao d : corsiDao){
            if(!Arrays.stream(ids).anyMatch(i -> i == d.getDocente())){
                if(d.getInterno() == 1) {
                    DipendentiDto temp = new DipendentiDto();
                    temp = this.dipendentiService.getById(d.getDocente());
                    dto.add(temp);
                    ids[k] = d.getDocente();
                    k++;
                }
            }
        }

        return ids;
    }

    @Override
    public int[] getDocentiInterniPages() {
        Integer items = this.corsiDocentiRepository.getDocentiInterniPages();
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    @Override
    public ArrayList<DipendentiDto> getDocentiInterni(int page) {
        page = (page - 1) * 10;

        int[] ids = this.getAllDocentiInterni();
        ArrayList<DipendentiDto> dto = new ArrayList<>();

        System.out.println("IDS: " + Arrays.toString(ids));

        for(int k = 0; k <= ids.length - 1 || k <= page; k++){
            if(ids[k] != 0) dto.add(this.dipendentiService.getById(ids[k]));
        }

        return dto;
    }

    /**
     * Filtri e paginazione docenti interni
     */
    @Override
    public int[] getDocentiInterniPagesFilterNome(String nome) {
        Integer items = this.corsiDocentiRepository.getPageFilterName(nome);
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    @Override
    public int[] getDocentiInterniPagesFilterCognome(String cognome) {
        Integer items = this.corsiDocentiRepository.getPageFilterCognome(cognome);
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    @Override
    public ArrayList<DipendentiDto> filterDocentiInterniByNome(String nome, int page) {
        page = (page - 1) * 10;

        List<CorsiDocentiDao> allCorDoc = this.corsiDocentiRepository.getAllCorsiDocenti();
        int[] ids = new int[allCorDoc.size()];
        ArrayList<DipendentiDto> dto = new ArrayList<>();

        int j = 0;
        for(CorsiDocentiDao d : allCorDoc){
            if(d.getInterno() == 0){
                if (!Arrays.stream(ids).anyMatch(k -> k == d.getDocente())){
                    ids[j] = d.getDocente();
                    j++;
                }
            }
        }

        System.out.println("    IDS    " + Arrays.toString(ids));

        j = 0;
        for(int i = page; i != page + 10 || i != ids.length; i++){
            System.out.println("    I    " + i);
            System.out.println("    IDS    " + ids[i]);
            dto.add(this.dipendentiService.filterDocIntByNome(ids[i], nome));
            //dto.add(this.dipendentiService.getById(d.getDipendente_id()));
        }

        return dto;
    }

    @Override
    public ArrayList<DipendentiDto> filterDocentiInterniByCognome(String cognome, int page) {
        page = (page - 1) * 10;

        List<CorsiDocentiDao> dao = this.corsiDocentiRepository.getDocentiInterni(page, 10);
        ArrayList<DipendentiDto> dto = new ArrayList<>();

        int[] ids = new int[dao.size()];

        int k = 0;
        for(CorsiDocentiDao d : dao){
            if(!Arrays.stream(ids).anyMatch(i -> i == d.getDocente())){
                if(d.getInterno() == 0) {
                    DipendentiDto temp = new DipendentiDto();
                    temp = this.dipendentiService.getById(d.getDocente());
                    if(temp.getCognome().toUpperCase().equals(cognome.toUpperCase())) {
                        dto.add(temp);
                    }
                    ids[k] = d.getDocente();
                    k++;
                }
            }
        }

        return dto;
    }

    @Override
    public boolean getRelazioni(int tipo, int id) {
        if(this.corsiDocentiRepository.getByDocenteAndTipo(tipo, id).size() > 0) return true;
        return false;
    }
}
