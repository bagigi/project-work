package it.its.projectwork.api.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.text.SimpleDateFormat;
import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CorsiDto {
    @Column(name = "corsoId")
    private int id;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "tipologia")
    private String tipologia;

    @Column(name = "descrizione")
    private String descrizione;

    @Column(name = "edizione")
    private String edizione;

    @Column(name = "previsto")
    private Integer previsto;

    @Column(name = "erogato")
    private Integer erogato;

    @Column(name = "durata")
    private Integer durata;

    @Column(name = "misura")
    private String misura;

    @Column(name = "note")
    private String note;

    @Column(name = "luogo")
    private String luogo;

    @Column(name = "ente")
    private String ente;

    @Column(name = "dataErogazione")
    private String data_erogazione;

    @Column(name = "dataChiusura")
    private String data_chiusura;

    @Column(name = "interno")
    private Integer interno;
}
