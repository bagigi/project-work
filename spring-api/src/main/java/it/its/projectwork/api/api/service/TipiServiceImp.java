package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.TipiDao;
import it.its.projectwork.api.api.repository.TipiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TipiServiceImp implements TipiService {
    @Autowired
    TipiRepository tipiRepository;

    @Override
    public List<TipiDao> getAll() {
        List<TipiDao> dao = this.tipiRepository.findAll();
        return dao;
    }
}
