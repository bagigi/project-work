package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.TipiDao;

import java.util.List;

public interface TipiService {
    public List<TipiDao> getAll();
}
