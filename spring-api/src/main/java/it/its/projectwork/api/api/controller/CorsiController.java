package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dao.CorsiDao;
import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.dto.CorsiDto;
import it.its.projectwork.api.api.service.CorsiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping(value = "api/corsi")
public class CorsiController {

    @Autowired
    CorsiService corsiService;


    @GetMapping(value = "/{id}", produces = "application/json")
    public BaseResponseDto<CorsiDto> getById(@PathVariable("id") int id){
        BaseResponseDto<CorsiDto> responseDto = new BaseResponseDto<>();

        CorsiDto dto = this.corsiService.getById(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @GetMapping(value = "/info/pages", produces = "application/json")
    public BaseResponseDto<CorsiDto> getCorsiPage(){
        BaseResponseDto<CorsiDto> responseDto = new BaseResponseDto<>();

        int[] pages = this.corsiService.getCorsiPages();
        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(pages);

        return responseDto;
    }

    @GetMapping(produces = "application/json", value = "/page/{page}")
    public BaseResponseDto<ArrayList<CorsiDto>> getCorsiByPage(@PathVariable("page") int page){
        BaseResponseDto<ArrayList<CorsiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<CorsiDto> dto = this.corsiService.getCorsi(page);
        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    /**
     * @param corso corso completo (ricordarsi di mettere l' id del docente/dipendente)
     * @param tipoDocente 0 -> interno   1 -> esterno
     * @return
     */
    @PostMapping(produces = "application/json", value = "/{tipoDocente}/{idDocente}")
    public BaseResponseDto<CorsiDto> newCorso(
            @RequestBody CorsiDao corso,
            @PathVariable("tipoDocente") int tipoDocente,
            @PathVariable("idDocente") int idDocente
    ) throws ParseException {
        System.out.println("corso in arrivo: " + corso.toString());
        BaseResponseDto<CorsiDto> responseDto = new BaseResponseDto<>();

        Integer dto = this.corsiService.newCorso(corso, tipoDocente, idDocente);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @PatchMapping(value = "/{tipoDocente}", produces = "application/json", consumes = "application/json")
    public BaseResponseDto<CorsiDto> update(@RequestBody CorsiDao corso, @PathVariable("tipoDocente") int tipoDocente){
        BaseResponseDto<CorsiDto> responseDto = new BaseResponseDto<>();

        this.corsiService.updateCorso(corso, tipoDocente);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(null);

        return responseDto;
    }

    @GetMapping(value = "/tipologia/{idTipologia}", produces = "application/json")
    public BaseResponseDto<ArrayList<CorsiDto>> getByTipologia(@PathVariable("idTipologia") int id){
        BaseResponseDto<ArrayList<CorsiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<CorsiDto> dto = this.corsiService.getCorsiByTipologia(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public BaseResponseDto<CorsiDto> deleteById(@PathVariable("id") int id){
        BaseResponseDto<CorsiDto> responseDto = new BaseResponseDto<>();

        this.corsiService.deleteById(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(null);

        return responseDto;
    }

    @GetMapping(value = "/info/docente/{idCorso}", produces = "application/json")
    public BaseResponseDto<Integer> getDocenteId(@PathVariable("idCorso") int id){
        BaseResponseDto<Integer> responseDto = new BaseResponseDto<>();

        Integer docente = this.corsiService.getDocenteId(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(docente);

        return responseDto;
    }

    @GetMapping(value = "/info/docente/tipo/{idCorso}", produces = "application/json")
    public BaseResponseDto<Integer> getTipoDocente(@PathVariable("idCorso") int id){
        BaseResponseDto<Integer> responseDto = new BaseResponseDto<>();

        Integer docente = this.corsiService.getTipoDocente(id);
        System.out.println("tipo docente: " + docente);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(docente);

        return responseDto;
    }

    @GetMapping(value = "/info/argomento/{idCorso}", produces = "application/json")
    public BaseResponseDto<Integer> getArgomento(@PathVariable("idCorso") int id){
        BaseResponseDto<Integer> responseDto = new BaseResponseDto<>();

        Integer argomento = this.corsiService.getArgomento(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(argomento);

        return responseDto;
    }

    @GetMapping(value = "/info/page/alunno/{id}", produces = "application/json")
    public BaseResponseDto<int[]> getByAlunnoPages(@PathVariable("id") int id){
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.corsiService.getAlunnoPage(id));

        return responseDto;
    }

    @GetMapping(value = "/alunno/{id}/{page}", produces = "application/json")
    public BaseResponseDto<ArrayList<CorsiDto>> getByAlunno(
            @PathVariable("id") int alunno,
            @PathVariable("page") int page
    ) {
        BaseResponseDto<ArrayList<CorsiDto>> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.corsiService.getCorsoByAlunno(alunno, page));

        return responseDto;
    }

    @GetMapping(value = "/info/relazioni/{id}", produces = "application/json")
    public BaseResponseDto<Boolean> getRelazioni(@PathVariable("id") int id){
        BaseResponseDto<Boolean> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.corsiService.getRelazioni(id));

        return responseDto;
    }
}
