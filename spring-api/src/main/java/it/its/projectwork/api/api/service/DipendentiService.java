package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dto.DipendentiDto;

import java.util.ArrayList;

public interface DipendentiService {
    public ArrayList<DipendentiDto> getAll();
    public DipendentiDto getByMatricola(int matricola);
    public ArrayList<DipendentiDto> getByNomeCognomeMatricola(String value);
    public DipendentiDto getById(int dipendenteId);
    public DipendentiDto filterDocIntByNome(int id, String nome);

    /**
     * Paginazione
     */
    public int[] getPages();
    public ArrayList<DipendentiDto> getByPage(int page);

    /**
     * Filtri
     */
    public ArrayList<DipendentiDto> filterByNome(String nome, int page);
    public ArrayList<DipendentiDto> filterByCognome(String cognome, int page);

    /**
     * Paginazione filtri
     */
    public int[] pageFilterNome(String nome);
    public int[] pageFilterCognome(String cognome);
}
