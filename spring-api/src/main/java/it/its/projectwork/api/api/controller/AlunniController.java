package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dao.CorsiAlunniDao;
import it.its.projectwork.api.api.dto.AlunniMultipli;
import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.dto.CorsiAlunniDto;
import it.its.projectwork.api.api.service.CorsiAlunniService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "api/alunni")
public class AlunniController {
    /**
     * Nuove aggiunte
     */

    @Autowired
    CorsiAlunniService corsiAlunniService;

    @GetMapping(value = "/info/pages", produces = "application/json")
    public BaseResponseDto<Integer> getPages(){
        BaseResponseDto<Integer> responseDto = new BaseResponseDto<>();

        int[]  pages = this.corsiAlunniService.getPages();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(pages);

        return responseDto;
    }

    @PostMapping(produces = "application/json", value = "/{idCorso}/{idAlunno}")
    public BaseResponseDto<CorsiAlunniDao> addAlunnoToCorso(@PathVariable("idCorso") int idCorso, @PathVariable("idAlunno") int idAlunno){
        BaseResponseDto<CorsiAlunniDao> responseDto = new BaseResponseDto<>();

        CorsiAlunniDao dao = this.corsiAlunniService.insertAlunno(idCorso, idAlunno);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dao);

        return responseDto;
    }

    @GetMapping(produces = "application/json", value="/page/{page}")
    public BaseResponseDto<List<CorsiAlunniDao>> getByPage(@PathVariable("page") int page){
        BaseResponseDto<List<CorsiAlunniDao>> responseDto = new BaseResponseDto<>();

        List<CorsiAlunniDao> dao = this.corsiAlunniService.getByPage(page);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dao);

        return responseDto;
    }

    @PostMapping(produces = "application/json", value = "/{idCorso}")
    public BaseResponseDto<CorsiAlunniDao> multipleInsert(
            @PathVariable("idCorso") int idCorso,
            @RequestBody AlunniMultipli listaAlunni
            ){
        BaseResponseDto<List<CorsiAlunniDao>> responseDto = new BaseResponseDto<>();

        System.out.println("lista di alunni: " + Arrays.toString(listaAlunni.getAlunni()));

        this.corsiAlunniService.insertMultipleAlunni(idCorso, listaAlunni.getAlunni());

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        //responseDto.setResponse(dao);
        return null;
    }

    @GetMapping(produces = "application/json", value = "/corso/{idCorso}")
    public BaseResponseDto<ArrayList<CorsiAlunniDto>> getByCorsoId(@PathVariable("idCorso") int id){
        BaseResponseDto<ArrayList<CorsiAlunniDto>> responseDto = new BaseResponseDto<>();

        ArrayList<CorsiAlunniDto> alunni = this.corsiAlunniService.getByCorsoId(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(alunni);

        return responseDto;
    }

    @DeleteMapping(produces = "application/json", value = "/{idDipendente}/{idCorso}")
    public BaseResponseDto<CorsiAlunniDto> delete(@PathVariable("idDipendente") int idDipendente, @PathVariable("idCorso") int idCorso){
        BaseResponseDto<CorsiAlunniDto> responseDto = new BaseResponseDto<>();

        this.corsiAlunniService.deleteRow(idDipendente, idCorso);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }
}
