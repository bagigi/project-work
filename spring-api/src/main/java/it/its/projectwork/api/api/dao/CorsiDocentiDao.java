package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "corsi_docenti")
@AllArgsConstructor
@NoArgsConstructor

@Data
public class CorsiDocentiDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "corsoDocentiId")
    private Integer id;

    @Column(name = "corso")
    private Integer corso;

    @Column(name = "docente")
    private Integer docente;

    @Column(name = "interno")
    private Integer interno;
}
