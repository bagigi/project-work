package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.MisureDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MisureRepository extends JpaRepository<MisureDao, Integer> {
}
