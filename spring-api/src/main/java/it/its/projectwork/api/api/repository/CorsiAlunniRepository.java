package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.CorsiAlunniDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CorsiAlunniRepository extends JpaRepository<CorsiAlunniDao, Integer> {
    /**
     * Ritorna il numero delle pagine
     */
    @Query(nativeQuery = true, value = "select count(*) from corsi_utenti")
    int getPages();

    /**
     * Ritorna la specifica pagina
     */
    @Query(nativeQuery = true, value = "select * from corsi_utenti limit ?1, ?2")
    List<CorsiAlunniDao> getByPage(int start, int limit);

    void deleteByCorso(int corso);

    @Query(nativeQuery = true, value = "select * from corsi_utenti where corso = ?1")
    CorsiAlunniDao[] getByCorsoId(int corsoId);

    @Query(nativeQuery = true, value = "select corsi_utenti_id from corsi_utenti order by corsi_utenti_id desc limit 1")
    Integer getLastId();

    @Modifying
    @Query(nativeQuery = true, value = "delete from corsi_utenti where dipendente = ?1 and corso = ?2")
    void deleteRow(int idDipendente, int idCorso);
}
