package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "corsi")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CorsiDao {
    @Id
    @Column(name = "corsoId")
    private int corso_id;

    /*@ManyToOne
    @JoinColumn(name = "tipo", foreignKey = @ForeignKey(name = "corsi_ibfk_2"))
    private TipiDao tipo;*/

    @Column(name = "tipo")
    private Integer tipo;

    @Column(name = "tipologia")
    private Integer tipologia;

    @Column(name = "descrizione")
    private String descrizione;

    @Column(name = "edizione")
    private String edizione;

    @Column(name = "previsto")
    private Integer previsto;

    @Column(name = "erogato")
    private Integer erogato;

    @Column(name = "durata")
    private Integer durata;

    @Column(name = "misura")
    private Integer misura;

    @Column(name = "note")
    private String note;

    @Column(name = "luogo")
    private String luogo;

    @Column(name = "ente")
    private Integer ente;

    @Column(name = "dataErogazione")
    private String data_erogazione;

    @Column(name = "dataChiusura")
    private String data_chiusura;

    @Column(name = "dataCensimento")
    private Date data_censimento;

    @Column(name = "interno")
    private Integer interno;
}
