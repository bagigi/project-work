package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.ValutazioniUtentiDao;
import it.its.projectwork.api.api.repository.ValutazioniUtentiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ValutazioniUtentiServiceImp implements ValutazioniUtentiService {
    @Autowired
    ValutazioniUtentiRepository valutazioniUtentiRepository;

    @Override
    public void inserisciValutazione(ValutazioniUtentiDao dao) {
        this.valutazioniUtentiRepository.saveAndFlush(dao);
    }

    @Override
    public ValutazioniUtentiDao getValutazioneByCorso(int dipendente, int corso) {
        return this.valutazioniUtentiRepository.getValutazioneByCorso(dipendente, corso);
    }

    @Override
    public ArrayList<ValutazioniUtentiDao> getValutazioniDipendenti(int id, int page) {
        page = (page - 1)* 10;
        List<ValutazioniUtentiDao> dao = this.valutazioniUtentiRepository.getValutazioniUtenti(id, page, 10);
        ArrayList<ValutazioniUtentiDao> lista = new ArrayList<>();

        for(ValutazioniUtentiDao d : dao) lista.add(d);

        return lista;
    }

    /**
     * Paginazione
     */
    @Override
    public int[] getPagesUtentiAll() {
        Integer items = this.valutazioniUtentiRepository.getPageUtentiAll();
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    /**
     * Eliminazione
     */
    @Override
    public boolean elimina(int id) {
        this.valutazioniUtentiRepository.deleteById(id);
        if(this.valutazioniUtentiRepository.findById(id).isPresent()) return false;
        else return true;
    }
}
