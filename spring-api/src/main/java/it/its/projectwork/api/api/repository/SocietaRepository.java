package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.SocietaDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SocietaRepository extends JpaRepository<SocietaDao, Integer> {

}
