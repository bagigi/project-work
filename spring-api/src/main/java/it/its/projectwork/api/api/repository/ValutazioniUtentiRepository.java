package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.ValutazioniUtentiDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ValutazioniUtentiRepository extends JpaRepository<ValutazioniUtentiDao, Integer> {
    /**
     * Query generale
     * @param id
     * @return
     */
    @Query(nativeQuery = true, value = "select * from valutazione_utenti where dipendente = ?1 limit ?2, ?3")
    List<ValutazioniUtentiDao> getValutazioniUtenti(int id, int start, int stop);

    /**
     * Query specifica
     * @param dipendente
     * @param corso
     * @return
     */
    @Query(nativeQuery = true, value = "select * from valutazione_utenti where dipendente = ?1 and corso = ?2")
    ValutazioniUtentiDao getValutazioneByCorso(int dipendente, int corso);

    /**
     * Paginazione
     */
    @Query(nativeQuery = true, value = "select count(*) from valutazione_utenti")
    Integer getPageUtentiAll();

    void deleteByCorso(int idCorso);

    @Query(nativeQuery = true, value = "select * from valutazione_utenti where corso = ?1")
    List<ValutazioniUtentiDao> getByCorso(int idCorso);
}
