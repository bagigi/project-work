package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.CorsiDocentiDao;
import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dao.DocentiDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CorsiDocentiRepository extends JpaRepository<CorsiDocentiDao, Integer> {
    @Query(nativeQuery = true, value = "select * from dipendenti where dipendente_id = ?1")
    DipendentiDao checkDipendente(int matricola);

    @Query(nativeQuery = true, value = "select * from docenti where docente_id = ?1")
    DocentiDao checkDocente(int id);

    @Query(nativeQuery = true, value = "select docente from corsi_docenti where corso = ?1")
    int getDocenteId(int corsoId);

    @Query(nativeQuery = true, value = "select interno from corsi_docenti where corso = ?1")
    int getDocenteTipo(int corsoId);

    @Query(nativeQuery = true, value = "select * from corsi_docenti")
    List<CorsiDocentiDao> getAllCorsiDocenti();

    void deleteByCorso(int corso);

    @Modifying
    @Query(nativeQuery = true, value = "update corsi_docenti set docente = ?1, interno = ?2 where corso = ?3 ")
    void updateDocente(int docenteId, int tipoDocente, int corsoId);

    /**
     * DOCENTI INTERNI
     * Paginazione
     */
    @Query(nativeQuery = true, value = "select count(*) from corsi_docenti where interno = 0")
    Integer getDocentiInterniPages();

    @Query(nativeQuery = true, value = "select * from corsi_docenti limit ?1, ?2")
    List<CorsiDocentiDao> getDocentiInterni(int start, int stop);

    /**
     * Filtri
     */
    @Query(nativeQuery = true, value = "select count(*) from dipendenti where nome = ?1")
    Integer getPageFilterName(String nome);
    @Query(nativeQuery = true, value = "select * from dipendenti" +
            " where dipendente_id = ?1"
    )
    List<DipendentiDao> filterByNome(int id);

    @Query(nativeQuery = true, value = "select count(*) from dipendenti where cognome = ?1")
    Integer getPageFilterCognome(String nome);
    @Query(nativeQuery = true, value = "select * from dipendenti where cognome = %?1% limit ?2, ?3")
    List<DipendentiDao> filterByCognome(String nome, int start, int stop);

    /**
     * Valutazioni
     */
    @Query(nativeQuery = true, value = "select corso from corsi_docenti where docente = ?1 and interno = 0")
    int[] getCorsiByDocenteInterno(int docente);
    @Query(nativeQuery = true, value = "select corso from corsi_docenti where docente = ?1 and interno = 1")
    int[] getCorsiByDocenteEsterno(int docente);

    @Query(nativeQuery = true, value = "select * from corsi_docenti where docente = ?1 and interno = ?2")
    List<CorsiDocentiDao> getByDocenteAndTipo(int id, int tipo);

    @Query(nativeQuery = true, value = "select * from corsi_docenti where corso = ?1")
    List<CorsiDocentiDao> getByCorso(int corsoId);

    @Modifying
    @Query(nativeQuery = true, value = "delete from docenti where docente_id = ?1")
    void deleteByDocenteId(int ids);
}
