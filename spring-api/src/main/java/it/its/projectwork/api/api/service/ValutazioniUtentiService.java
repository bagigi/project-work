package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.ValutazioniUtentiDao;

import java.util.ArrayList;

public interface ValutazioniUtentiService {
    public void inserisciValutazione(ValutazioniUtentiDao dao);
    public ArrayList<ValutazioniUtentiDao> getValutazioniDipendenti(int id, int page);
    public ValutazioniUtentiDao getValutazioneByCorso(int dipendente, int corso);
    public int[] getPagesUtentiAll();
    public boolean elimina(int id);
}
