package it.its.projectwork.api.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CorsiAlunniDto {
    @Id
    @Column(name = "CorsiUtentiId")
    public int id;

    @Column(name = "corso")
    public int corso;

    @Column(name = "dipendente")
    public DipendentiDto dipendente;

    @Column(name = "durata")
    public int durata;
}
