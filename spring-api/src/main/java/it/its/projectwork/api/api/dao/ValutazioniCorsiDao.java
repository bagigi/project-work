package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "valutazioni_corsi")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ValutazioniCorsiDao {
    @Id
    @Column(name = "valutazioneCorsiId")
    public int id;

    @Column(name = "crescitaProfessionale")
    public int crescitaProfessionale;

    @Column(name = "lavoroAttuale")
    public int lavoro_attuale;

    @Column(name = "argomenti")
    public int argomenti;

    @Column(name = "approfondimento")
    public int approfondimento;

    @Column(name = "durata")
    public int durata;

    @Column(name = "divisioneTeoriaPratica")
    public int divisioneTeoriaPratica;

    @Column(name = "esercitazioni")
    public int esercitazioni;

    @Column(name = "materialeDidattico")
    public int materiale;

    @Column(name = "responsabile")
    public int responsabile;

    @Column(name = "media")
    public float media;

    @Column(name = "corso")
    public int corso;
}
