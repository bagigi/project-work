package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.TipologieDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipologieRepository extends JpaRepository<TipologieDao, Integer> {
}
