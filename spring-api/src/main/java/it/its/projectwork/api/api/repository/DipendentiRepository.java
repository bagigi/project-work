package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dto.DipendentiDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface DipendentiRepository extends JpaRepository<DipendentiDao, Integer> {
    Optional<DipendentiDao> findByMatricola(int matricola);

    /**
     * Paginazione
     */
    @Query(nativeQuery = true, value = "select count(*) from dipendenti")
    public Integer getPages();
    @Query(nativeQuery = true, value = "select * from dipendenti limit ?1, ?2")
    public ArrayList<DipendentiDao> getByPage(int start, int stop);

    /**
     * Filtro per nome
     * @param nome
     * @return
     */
    @Query(nativeQuery = true, value = "select count(*) from dipendenti where nome like %?1%")
    Integer getPagesFilterByNome(String nome);
    @Query(nativeQuery = true, value = "select * from dipendenti where nome like %?1% limit ?2, ?3")
    List<DipendentiDao> filterByNome(String nome, int start, int stop);

    /**
     * Filtro per cognome
     * @param cognome
     * @return
     */
    @Query(nativeQuery = true, value = "select count(*) from dipendenti where cognome like %?1%")
    Integer getPagesFilterByCognome(String cognome);
    @Query(nativeQuery = true, value = "select * from dipendenti where cognome like %?1% limit ?2, ?3")
    List<DipendentiDao> filterByCognome(String cognome, int start, int stop);

    /**
     * Filtro per aggiunta alunni/docenti
     * @param value
     * @return
     */
    @Query(nativeQuery = true, value = "select * from dipendenti where nome like %?1% or cognome like %?1% or matricola like %?1%")
    ArrayList<DipendentiDao> findByMatricolaNomeCognome(String value);

    @Query(nativeQuery = true, value = "select * from dipendenti" +
            " inner join corsi_docenti on dipendenti.dipendente_id = corsi_docenti.docente" +
            " where corsi_docenti.docente = ?1" +
            " and where nome = ?2")
    Optional<DipendentiDao> filtraDocenteInternoByNome(int id, String valore);

    @Query(nativeQuery = true, value = "select * from dipendenti" +
            " inner join corsi_docenti on dipendenti.dipendente_id = corsi_docenti.docente" +
            " where corsi_docenti.docente = ?1" +
            " and where cognome = ?2")
    Optional<DipendentiDao> filtraDocenteInternoByCognome(int id, String valore);

    @Query(nativeQuery = true, value = "select * from dipendenti where dipendente_id = ?1 and nome like %?2%")
    Optional<DipendentiDao> filtraByNomeAndId(int id, String nome);
}
