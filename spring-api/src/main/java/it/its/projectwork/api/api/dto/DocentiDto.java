package it.its.projectwork.api.api.dto;

import it.its.projectwork.api.api.dao.DipendentiDao;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
public class DocentiDto {
    private int id;
    private String titolo;
    private String ragione_sociale;
    private String nome;
    private String cognome;
    private long telefono;
    private long fax;
    private String partita_iva;
    private String referente;

}
