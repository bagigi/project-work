package it.its.projectwork.api.api.service;

import com.sun.org.apache.regexp.internal.RE;
import it.its.projectwork.api.api.dao.*;
import it.its.projectwork.api.api.dto.CorsiDto;
import it.its.projectwork.api.api.dto.DocentiDto;
import it.its.projectwork.api.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class CorsiServiceImp  implements CorsiService{

    @Autowired
    CorsiRepository corsiRepository;
    @Autowired
    CorsiDocentiRepository corsiDocentiRepository;
    @Autowired
    DipendentiRepository dipendentiRepository;
    @Autowired
    DocentiRepository docentiRepository;
    @Autowired
    TipologieRepository tipologieRepository;
    @Autowired
    MisureRepository misureRepository;
    @Autowired
    TipiRepository tipiRepository;
    @Autowired
    SocietaRepository societaRepository;
    @Autowired
    CorsiAlunniRepository corsiAlunniRepository;
    @Autowired
    ValutazioniDocentiRepository valutazioniDocentiRepository;
    @Autowired
    ValutazioniCorsiRepository valutazioniCorsiRepository;
    @Autowired
    ValutazioniUtentiRepository valutazioniUtentiRepository;

    @Override
    public Integer newCorso(CorsiDao corso, int tipo, int idDocente) throws ParseException {
        if(tipo == 0){
            //DipendentiDao d = this.corsiDocentiRepository.checkDipendente(corso.getErogato());
            Optional<DipendentiDao> d = this.dipendentiRepository.findById(idDocente);
            if(!d.isPresent()) return null;
            Optional<SocietaDao> societaDao = this.societaRepository.findById(d.get().getSocieta());
            if(!societaDao.isPresent()) return null;
            corso.setEnte(societaDao.get().getId());
        } else {
            //DocentiDao d = this.corsiDocentiRepository.checkDocente(corso.getErogato());
            Optional<DocentiDao> d = this.docentiRepository.findById(idDocente);
            if(!d.isPresent()) return null;
            corso.setEnte(-1);
        }

        System.out.println("Corso: " + corso);

        this.corsiRepository.saveAndFlush(corso);
        int idCorso = this.corsiRepository.getUltimoCorso().getCorso_id();

        CorsiDocentiDao corsoDocente = new CorsiDocentiDao();
        CorsiDao d = this.corsiRepository.getUltimoCorso();
        //if(d != null) System.out.println("Corso: " + d.toString());

        corsoDocente.setDocente(idDocente);
        corsoDocente.setCorso(d.getCorso_id());
        corsoDocente.setInterno(d.getInterno());
        corsoDocente.setInterno(tipo);

        this.corsiDocentiRepository.saveAndFlush(corsoDocente);

        return idCorso;
    }

    @Override
    public int[] getCorsiPages() {
        int items = this.corsiRepository.getCorsiPages();

        int pages = items/10;
        if(items%10 != 0) pages++;

        int[] kk = new int[pages];
        for(int i = 0; i != pages; i++){
            kk[i] = i+1;
        }

        return kk;
    }

    @Override
    public ArrayList<CorsiDto> getCorsi(int page) {
        page = (page - 1) * 10;

        ArrayList<CorsiDao> dao = this.corsiRepository.getCorsiByPage(page, page + 10);
        ArrayList<CorsiDto> lista = new ArrayList<>();

        for(CorsiDao d : dao){
            Optional<MisureDao> misura = this.misureRepository.findById(d.getMisura());
            Optional<TipologieDao> tipologia = this.tipologieRepository.findById(d.getTipologia());
            Optional<TipiDao> tipo = this.tipiRepository.findById(d.getTipo());
            int docenteId = this.corsiDocentiRepository.getDocenteId(d.getCorso_id());
            int docenteTipo = this.corsiDocentiRepository.getDocenteTipo(d.getCorso_id());
            String docente = this.setDocente(docenteId, docenteTipo);

            CorsiDto dto = new CorsiDto();
            dto.setId(d.getCorso_id());
            dto.setData_chiusura(d.getData_chiusura());
            dto.setData_erogazione(d.getData_erogazione());
            dto.setDescrizione(d.getDescrizione());
            dto.setDurata(d.getDurata());
            dto.setEdizione(d.getEdizione());
            dto.setEnte(docente);
            dto.setErogato(d.getErogato());
            dto.setInterno(d.getInterno());
            dto.setNote(d.getNote());
            dto.setLuogo(d.getLuogo());
            dto.setMisura(misura.get().getDescrizione());
            dto.setPrevisto(d.getPrevisto());
            dto.setTipo(tipo.get().getDescrizione());
            dto.setTipologia(tipologia.get().getDescrizione());
            lista.add(dto);
        }

        return lista;
    }

    @Override
    public ArrayList<CorsiDto> getCorsiByTipologia(int tipologia) {
        ArrayList<CorsiDto> lista = new ArrayList<>();
        ArrayList<CorsiDao> dao = this.corsiRepository.findByTipologia(tipologia);

        for(CorsiDao d : dao){
            Optional<MisureDao> misura = this.misureRepository.findById(d.getMisura());
            Optional<TipiDao> tipo = this.tipiRepository.findById(d.getTipo());
            Optional<TipologieDao> tip = this.tipologieRepository.findById(tipologia);
            int docenteId = this.corsiDocentiRepository.getDocenteId(d.getCorso_id());
            int docenteTipo = this.corsiDocentiRepository.getDocenteTipo(d.getCorso_id());
            String docente = this.setDocente(docenteId, docenteTipo);

            CorsiDto dto = new CorsiDto();
            dto.setId(d.getCorso_id());
            dto.setData_chiusura(d.getData_chiusura());
            dto.setData_erogazione(d.getData_erogazione());
            dto.setDescrizione(d.getDescrizione());
            dto.setDurata(d.getDurata());
            dto.setEdizione(d.getEdizione());
            dto.setEnte(docente);
            dto.setErogato(d.getErogato());
            dto.setInterno(d.getInterno());
            dto.setNote(d.getNote());
            dto.setLuogo(d.getLuogo());
            dto.setMisura(misura.get().getDescrizione());
            dto.setPrevisto(d.getPrevisto());
            dto.setTipo(tipo.get().getDescrizione());
            dto.setTipologia(tip.get().getDescrizione());
            lista.add(dto);
        }

        return lista;
    }

    @Override
    public boolean getRelazioni(int idCorso) {
        if (this.valutazioniCorsiRepository.getByCorsoId(idCorso).isPresent()) return true;
        if (this.valutazioniUtentiRepository.getByCorso(idCorso).size() > 0) return true;
        if (this.valutazioniDocentiRepository.getByCorso(idCorso).size() > 0) return true;
        if (this.corsiDocentiRepository.getByCorso(idCorso).size() > 0) return true;
        if (this.corsiAlunniRepository.getByCorsoId(idCorso).length > 0) return true;
        return false;
    }

    @Override
    public void deleteById(int id) {
        this.valutazioniDocentiRepository.deleteByCorso(id);
        this.valutazioniUtentiRepository.deleteByCorso(id);
        this.valutazioniCorsiRepository.deleteByCorso(id);
        this.corsiDocentiRepository.deleteByCorso(id);
        this.corsiAlunniRepository.deleteByCorso(id);
        this.corsiRepository.deleteById(id);
        return;
    }

    @Override
    public CorsiDto getById(int id) {
        Optional<CorsiDao> opt = this.corsiRepository.findById(id);

        System.out.println("corso: " + opt.get().toString());

        if(opt.isPresent()){
            CorsiDto dto = new CorsiDto();
            CorsiDao d = opt.get();
            Optional<MisureDao> misura = this.misureRepository.findById(d.getMisura());
            Optional<TipiDao> tipo = this.tipiRepository.findById(d.getTipo());
            Optional<TipologieDao> tip = this.tipologieRepository.findById(d.getTipologia());
            int docenteId = this.corsiDocentiRepository.getDocenteId(d.getCorso_id());
            int docenteTipo = this.corsiDocentiRepository.getDocenteTipo(d.getCorso_id());
            String docente = this.setDocente(docenteId, docenteTipo);

            dto.setId(d.getCorso_id());
            dto.setData_chiusura(d.getData_chiusura());
            dto.setData_erogazione(d.getData_erogazione());
            dto.setDescrizione(d.getDescrizione());
            dto.setDurata(d.getDurata());
            dto.setEdizione(d.getEdizione());
            dto.setEnte(docente);
            dto.setErogato(d.getErogato());
            dto.setInterno(d.getInterno());
            dto.setNote(d.getNote());
            dto.setLuogo(d.getLuogo());
            dto.setMisura(misura.get().getDescrizione());
            dto.setPrevisto(d.getPrevisto());
            dto.setTipo(tipo.get().getDescrizione());
            dto.setTipologia(tip.get().getDescrizione());

            return dto;
        }

        return null;
    }

    @Override
    public void updateCorso(CorsiDao corso, int tipoDocente) {
        System.out.println("CORSO: " + corso.toString());
        this.corsiDocentiRepository.updateDocente(corso.getEnte(), tipoDocente, corso.getCorso_id());
        //int tipologia = corso.getTipologia();
        this.corsiRepository.saveAndFlush(corso);
    }

    @Override
    public Integer getDocenteId(int corsoId) {
        return this.corsiDocentiRepository.getDocenteId(corsoId);
    }

    @Override
    public Integer getTipoDocente(int corsoId) {
        return this.corsiDocentiRepository.getDocenteTipo(corsoId);
    }

    @Override
    public Integer getArgomento(int corsoId) {
        return this.corsiRepository.findById(corsoId).get().getTipo();
    }

    @Override
    public int[] getAlunnoPage(int idAlunno) {
        int items = this.corsiRepository.getAlunnoPages(idAlunno);

        int pages = items/10;
        if(items%10 != 0) pages++;

        int[] kk = new int[pages];
        for(int i = 0; i != pages; i++){
            kk[i] = i+1;
        }

        return kk;
    }

    @Override
    public ArrayList<CorsiDto> getCorsoByAlunno(int idAlunno, int page) {
        page = (page - 1) * 10;
        List<CorsiDao> dao = this.corsiRepository.getCorsoByAlunno(idAlunno, page, page + 10);
        ArrayList<CorsiDto> lel = new ArrayList<>();

        for(CorsiDao d : dao){
            CorsiDto dto = new CorsiDto();
            Optional<MisureDao> misura = this.misureRepository.findById(d.getMisura());
            Optional<TipiDao> tipo = this.tipiRepository.findById(d.getTipo());
            Optional<TipologieDao> tip = this.tipologieRepository.findById(d.getTipologia());
            int docenteId = this.corsiDocentiRepository.getDocenteId(d.getCorso_id());
            int docenteTipo = this.corsiDocentiRepository.getDocenteTipo(d.getCorso_id());
            String docente = this.setDocente(docenteId, docenteTipo);

            dto.setId(d.getCorso_id());
            dto.setData_chiusura(d.getData_chiusura());
            dto.setData_erogazione(d.getData_erogazione());
            dto.setDescrizione(d.getDescrizione());
            dto.setDurata(d.getDurata());
            dto.setEdizione(d.getEdizione());
            dto.setEnte(docente);
            dto.setErogato(d.getErogato());
            dto.setInterno(d.getInterno());
            dto.setNote(d.getNote());
            dto.setLuogo(d.getLuogo());
            dto.setMisura(misura.get().getDescrizione());
            dto.setPrevisto(d.getPrevisto());
            dto.setTipo(tipo.get().getDescrizione());
            dto.setTipologia(tip.get().getDescrizione());

            lel.add(dto);
        }

        return lel;
    }

    private String setDocente(int id, int tipo){
        if(tipo == 0){
            Optional<DipendentiDao> dao = this.dipendentiRepository.findById(id);
            if(!dao.isPresent()) return null;
            String docente = dao.get().getCognome() + " " +dao.get().getNome();
            return docente;
        } else if(tipo == 1){
            Optional<DocentiDao> dao = this.docentiRepository.findById(id);
            if(!dao.isPresent()) return null;
            String docente = dao.get().getRagioneSociale();
            return docente;
        }
        return null;
    }
}
