package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dao.TipologieDao;
import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.service.TipologieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "api/tipologie")
public class TipologieController {
    @Autowired
    TipologieService tipologieService;

    @GetMapping(produces = "application/json")
    public BaseResponseDto<List<TipologieDao>> getAll(){
        BaseResponseDto<List<TipologieDao>> responseDto = new BaseResponseDto<>();

        List<TipologieDao> dao = this.tipologieService.getAll();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dao);

        return responseDto;
    }
}
