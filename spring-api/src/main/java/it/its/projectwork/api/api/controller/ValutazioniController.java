package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dao.ValutazioniCorsiDao;
import it.its.projectwork.api.api.dao.ValutazioniDocentiDao;
import it.its.projectwork.api.api.dao.ValutazioniUtentiDao;
import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.repository.ValutazioniCorsiRepository;
import it.its.projectwork.api.api.service.ValutazioniCorsiService;
import it.its.projectwork.api.api.service.ValutazioniDocentiService;
import it.its.projectwork.api.api.service.ValutazioniUtentiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = "api/valutazioni")
public class ValutazioniController {
    @Autowired
    ValutazioniUtentiService valutazioniUtentiService;
    @Autowired
    ValutazioniCorsiService valutazioniCorsiService;
    @Autowired
    ValutazioniDocentiService valutazioniDocentiService;

    @PostMapping(produces = "application/json", value = "/utente")
    public BaseResponseDto<ValutazioniUtentiDao> valutaUtente(@RequestBody ValutazioniUtentiDao dao){
        BaseResponseDto<ValutazioniUtentiDao> responseDto = new BaseResponseDto<>();

        this.valutazioniUtentiService.inserisciValutazione(dao);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(null);

        return responseDto;
    }

    @PostMapping(produces = "application/json", value = "/corso")
    public BaseResponseDto<ValutazioniCorsiDao> valutaCorso(@RequestBody ValutazioniCorsiDao dao){
        BaseResponseDto<ValutazioniCorsiDao> responseDto = new BaseResponseDto<>();

        this.valutazioniCorsiService.inserisciValutazione(dao);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(null);

        return responseDto;
    }

    @PostMapping(produces = "application/json", value = "/docente")
    public BaseResponseDto<ValutazioniDocentiDao> valutaDocente(@RequestBody ValutazioniDocentiDao dao){
        BaseResponseDto<ValutazioniDocentiDao> responseDto = new BaseResponseDto<>();

        this.valutazioniDocentiService.inserisciValutazione(dao);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(null);

        return responseDto;
    }

    /**
     * Get di TUTTE le valutazioni divise per UTENTI, DOCENTI, CORSI con paginazione
     * @param id
     * @return
     */

    @GetMapping(value = "/corso/{idCorso}", produces = "application/json")
    public BaseResponseDto<ValutazioniCorsiDao> getValutazioneCorso(@PathVariable("idCorso") int id){
        BaseResponseDto<ValutazioniCorsiDao> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniCorsiService.getByCorsoId(id));

        return responseDto;
    }

    @GetMapping(value = "/docente/{tipoD}/{idDocente}/{page}", produces = "application/json")
    public BaseResponseDto<ValutazioniDocentiDao> getValutazioniDocente(
            @PathVariable("tipoD") String tipo,
            @PathVariable("idDocente") int id,
            @PathVariable("page") int page
    ){
        BaseResponseDto<ValutazioniDocentiDao> responseDto = new BaseResponseDto<>();

        if(tipo.equals("interno")){
            responseDto.setResponse(this.valutazioniDocentiService.getValutazioniDocentiInterni(id, page));
        } else if(tipo.equals("esterno")){
            responseDto.setResponse(this.valutazioniDocentiService.getValutazioniDocentiEsterni(id, page));
        }

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }

    @GetMapping(value = "/dipendente/{idDipendente}/{page}", produces = "application/json")
    public BaseResponseDto<ValutazioniUtentiDao> getValutazioniUtente(
            @PathVariable("idDipendente") int id,
            @PathVariable("page") int page
    ){
        BaseResponseDto<ValutazioniUtentiDao> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniUtentiService.getValutazioniDipendenti(id, page));

        return responseDto;
    }

    /**
     * Valutazione specifica
     */
    @GetMapping(value = "/docente/{docente}/corso/{corso}", produces = "application/json")
    public BaseResponseDto<ValutazioniDocentiDao> getValutazioneDocenteByCorso(
            @PathVariable("docente") int docenteId,
            @PathVariable("corso") int corsoId
    ) {
        BaseResponseDto<ValutazioniDocentiDao> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniDocentiService.getByDocenteAndCorso(docenteId, corsoId));

        return responseDto;
    }

    @GetMapping(value = "/dipendente/{dipendente}/corso/{corso}", produces = "application/json")
    public BaseResponseDto<ValutazioniUtentiDao> getValutazioneDipendenteByCorso(
            @PathVariable("dipendente") int dipendente,
            @PathVariable("corso") int corso
    ){
        BaseResponseDto<ValutazioniUtentiDao> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniUtentiService.getValutazioneByCorso(dipendente, corso));

        return responseDto;
    }

    /**
     * Eliminazione
     */
    @DeleteMapping(value = "/{tipo}/{id}", produces = "application/json")
    public BaseResponseDto<Boolean> delete(
            @PathVariable("tipo") String tipo,
            @PathVariable("id") int id
    ) {
        BaseResponseDto<Boolean> responseDto = new BaseResponseDto<>();

        switch (tipo){
            case "alunno":
                responseDto.setResponse(this.valutazioniUtentiService.elimina(id));
                break;
            case "docente":
                responseDto.setResponse(this.valutazioniDocentiService.elimina(id));
                break;
            case "corso":
                responseDto.setResponse(this.valutazioniCorsiService.elimina(id));
                break;
            default:
                responseDto.setResponse(false);
        }

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }
}
