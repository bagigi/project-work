package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dto.DipendentiDto;
import it.its.projectwork.api.api.repository.DipendentiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DipendentiServiceImp implements DipendentiService {
    @Autowired
    DipendentiRepository dipendentiRepository;

    private List<DipendentiDao> getAllPrivate(){
        return this.dipendentiRepository.findAll();
    }

    private Optional<DipendentiDao> getByMatricolaPrivate(int matricola){
        return this.dipendentiRepository.findByMatricola(matricola);
    }

    @Override
    public int[] getPages() {
        Integer items = this.dipendentiRepository.getPages();
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    @Override
    public ArrayList<DipendentiDto> getByPage(int page) {
        page = (page - 1) * 10;
        System.out.println("PAGINA DEL CAZZO: " + page);
        List<DipendentiDao> dao = this.dipendentiRepository.getByPage(page, 10);

        ArrayList<DipendentiDto> dto = new ArrayList<DipendentiDto>();

        for(DipendentiDao d : dao) {
            DipendentiDto temp = new DipendentiDto();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());

            dto.add(temp);
        }

        return dto;
    }

    @Override
    public ArrayList<DipendentiDto> getAll() {
        List<DipendentiDao> dao = this.getAllPrivate();

        ArrayList<DipendentiDto> dto = new ArrayList<DipendentiDto>();

        for(DipendentiDao d : dao) {
            DipendentiDto temp = new DipendentiDto();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());

            dto.add(temp);
        }

        return dto;
    }

    @Override
    public DipendentiDto getByMatricola(int matricola) {
        Optional<DipendentiDao> dao = getByMatricolaPrivate(matricola);
        if(dao.isPresent()){
            DipendentiDao temp = dao.get();
            DipendentiDto dto = new DipendentiDto();

            dto.setId(temp.getDipendente_id());
            dto.setMatricola(temp.getMatricola());
            dto.setTipo_dipendente(temp.getTipo_dipendente());
            dto.setSocieta(temp.getSocieta());
            dto.setSesso(temp.getSesso());
            dto.setQualifica(temp.getQualifica());
            dto.setLivello(temp.getLivello());
            dto.setNome(temp.getNome());
            dto.setCognome(temp.getCognome());

            return dto;
        }
        return null;
    }

    @Override
    public DipendentiDto getById(int dipendenteId) {
        Optional<DipendentiDao> dao = this.dipendentiRepository.findById(dipendenteId);
        DipendentiDto temp = new DipendentiDto();
        if(dao.isPresent()){
            DipendentiDao d = dao.get();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());
        }
        return temp;
    }

    @Override
    public ArrayList<DipendentiDto> getByNomeCognomeMatricola(String value) {
        ArrayList<DipendentiDao> dao = this.dipendentiRepository.findByMatricolaNomeCognome(value);
        ArrayList<DipendentiDto> dto = new ArrayList<>();

        for(DipendentiDao d : dao) {
            DipendentiDto temp = new DipendentiDto();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());

            dto.add(temp);
        }

        return dto;
    }

    /**
     * Paginazione dei filtri
     */
    @Override
    public int[] pageFilterNome(String nome) {
        Integer items = this.dipendentiRepository.getPagesFilterByNome(nome);
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    @Override
    public int[] pageFilterCognome(String cognome) {
        Integer items = this.dipendentiRepository.getPagesFilterByCognome(cognome);
        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    /**
     * Filtri
     */
    @Override
    public ArrayList<DipendentiDto> filterByNome(String nome, int page) {
        page = (page - 1) * 10;

        List<DipendentiDao> dao = this.dipendentiRepository.filterByNome(nome, page, 10);
        ArrayList<DipendentiDto> dto = new ArrayList<>();

        for(DipendentiDao d : dao) {
            DipendentiDto temp = new DipendentiDto();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());

            dto.add(temp);
        }

        return dto;
    }

    @Override
    public ArrayList<DipendentiDto> filterByCognome(String cognome, int page) {
        page = (page - 1) * 10;

        List<DipendentiDao> dao = this.dipendentiRepository.filterByCognome(cognome, page, 10);
        ArrayList<DipendentiDto> dto = new ArrayList<>();

        for(DipendentiDao d : dao) {
            DipendentiDto temp = new DipendentiDto();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());

            dto.add(temp);
        }

        return dto;
    }

    @Override
    public DipendentiDto filterDocIntByNome(int id, String nome) {
        Optional<DipendentiDao> dao = this.dipendentiRepository.filtraByNomeAndId(id, nome);
        DipendentiDto temp = new DipendentiDto();

        if(dao.isPresent()){
            DipendentiDao d = dao.get();

            temp.setId(d.getDipendente_id());
            temp.setMatricola(d.getMatricola());
            temp.setCognome(d.getCognome());
            temp.setNome(d.getNome());
            temp.setLivello(d.getLivello());
            temp.setQualifica(d.getQualifica());
            temp.setSesso(d.getSesso());
            temp.setSocieta(d.getSocieta());
            temp.setTipo_dipendente(d.getTipo_dipendente());
        }

        return temp;
    }
}
