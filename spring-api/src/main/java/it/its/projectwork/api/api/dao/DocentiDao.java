package it.its.projectwork.api.api.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "docenti")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DocentiDao {
    @Id
    @Column(name = "docenteId")
    private int docentiId;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    /*@OneToOne
    @JoinColumn(name = "lollo", referencedColumnName = "dipendenteId")
    private DipendentiDao dip;*/

    @Column(name = "titolo")
    private String titolo;

    @Column(name = "ragioneSociale")
    private String ragioneSociale;

    @Column(name = "indirizzo")
    private String indirizzo;

    @Column(name = "localita")
    private String localita;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "nazione")
    private String nazione;

    @Column(name = "telefono")
    private long telefono;

    @Column(name = "fax")
    private long fax;

    @Column(name = "partitaIva")
    private String partita_iva;

    @Column(name = "referente")
    private String referente;
}
