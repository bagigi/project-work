package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.CorsiDao;
import it.its.projectwork.api.api.dto.CorsiDto;

import java.text.ParseException;
import java.util.ArrayList;

public interface CorsiService {
    Integer newCorso(CorsiDao corso, int tipo, int idDocente) throws ParseException;
    int[] getCorsiPages();
    ArrayList<CorsiDto> getCorsi(int page);
    ArrayList<CorsiDto> getCorsiByTipologia(int tipologia);
    void deleteById(int id);
    CorsiDto getById(int id);
    Integer getDocenteId(int corsoId);
    Integer getTipoDocente(int corsoId);
    Integer getArgomento(int corsoId);
    void updateCorso(CorsiDao corso, int tipoDocente);
    int[] getAlunnoPage(int idAlunno);
    ArrayList<CorsiDto>  getCorsoByAlunno(int idAlunno, int page);
    public boolean getRelazioni(int idCorso);
}
