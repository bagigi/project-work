package it.its.projectwork.api.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BaseResponseDto<T>{

    private Date timestamp;
    private int status;
    private String error;
    private String message;
    private Object response;
}