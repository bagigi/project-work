package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.TipologieDao;

import java.util.List;

public interface TipologieService {
    public List<TipologieDao> getAll();
}
