package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.ValutazioniCorsiDao;
import it.its.projectwork.api.api.dao.ValutazioniDocentiDao;

public interface ValutazioniCorsiService {
    public void inserisciValutazione(ValutazioniCorsiDao dao);
    public ValutazioniCorsiDao getByCorsoId(int corsoId);
    public int[] getPagesCorsoAll();
    public boolean elimina(int id);
}
