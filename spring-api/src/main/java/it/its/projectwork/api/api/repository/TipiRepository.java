package it.its.projectwork.api.api.repository;

import it.its.projectwork.api.api.dao.TipiDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipiRepository extends JpaRepository<TipiDao, Integer> {
}
