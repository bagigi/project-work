package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.service.ValutazioniCorsiService;
import it.its.projectwork.api.api.service.ValutazioniDocentiService;
import it.its.projectwork.api.api.service.ValutazioniUtentiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping(value = "api/valutazioni/info/page")
public class ValutazioniPaginazioniController {
    @Autowired
    ValutazioniUtentiService valutazioniUtentiService;
    @Autowired
    ValutazioniDocentiService valutazioniDocentiService;
    @Autowired
    ValutazioniCorsiService valutazioniCorsiService;

    /**
     * Paginazione Generale
     * @return
     */

    @GetMapping(value = "/utente/all", produces = "application/json")
    public BaseResponseDto<int[]> getPagesUtenteAll(){
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniUtentiService.getPagesUtentiAll());

        return responseDto;
    }

    @GetMapping(value = "/docente/all", produces = "application/json")
    public BaseResponseDto<int[]> getPagesDocenteAll(){
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniDocentiService.getPagesDocentiAll());

        return responseDto;
    }

    @GetMapping(value = "/corso/all", produces = "application/json")
    public BaseResponseDto<int[]> getPagesCorsoAll() {
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniCorsiService.getPagesCorsoAll());

        return responseDto;
    }

    /**
     * Paginazione in base a dipendente
     */
    @GetMapping(value = "/alunno/{idAlunno}", produces = "application/json")
    public BaseResponseDto<int[]> getPagesAlunno(@PathVariable("idAlunno") int idAlunno){
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }

    @GetMapping(value = "/docente/{tipo}/{idDocente}", produces = "application/json")
    public BaseResponseDto<int[]> getPagesDocente(
            @PathVariable("tipo") int tipo,
            @PathVariable("idDocente") int idDocente
    ) {
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.valutazioniDocentiService.getPagesByDocenteId(idDocente, tipo));

        return responseDto;
    }
}
