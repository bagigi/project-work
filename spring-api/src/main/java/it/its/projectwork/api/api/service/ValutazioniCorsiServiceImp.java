package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.ValutazioniCorsiDao;
import it.its.projectwork.api.api.dao.ValutazioniDocentiDao;
import it.its.projectwork.api.api.repository.ValutazioniCorsiRepository;
import it.its.projectwork.api.api.repository.ValutazioniUtentiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ValutazioniCorsiServiceImp implements ValutazioniCorsiService{
    @Autowired
    ValutazioniCorsiRepository valutazioniRepository;

    @Override
    public void inserisciValutazione(ValutazioniCorsiDao dao) {
        this.valutazioniRepository.saveAndFlush(dao);
    }

    @Override
    public ValutazioniCorsiDao getByCorsoId(int corsoId) {
        if (this.valutazioniRepository.getByCorsoId(corsoId).isPresent()) return this.valutazioniRepository.getByCorsoId(corsoId).get();
        else return null;
    }

    /**
     * Paginazione
     */
    @Override
    public int[] getPagesCorsoAll() {
        Integer items = this.valutazioniRepository.getPagesCorsoAll();

        int pages = items / 10;
        if (items % 10 != 0)
            pages++;

        int[] kk = new int[pages];
        for (int i = 0; i != pages; i++) {
            kk[i] = i + 1;
        }

        return kk;
    }

    /**
     * Eliminazione
     */
    @Override
    public boolean elimina(int id) {
        this.valutazioniRepository.deleteById(id);
        if(this.valutazioniRepository.findById(id).isPresent()) return false;
        else return true;
    }
}
