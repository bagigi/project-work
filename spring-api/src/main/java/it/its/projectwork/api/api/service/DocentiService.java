package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.DipendentiDao;
import it.its.projectwork.api.api.dao.DocentiDao;
import it.its.projectwork.api.api.dto.DipendentiDto;
import it.its.projectwork.api.api.dto.DocentiDto;

import java.util.ArrayList;

public interface DocentiService {
    public ArrayList<DocentiDto> getAll();

    public DocentiDto getById(int id);

    public DocentiDto create(DocentiDao docente);

    public boolean delete(int id);

    public DocentiDao update(DocentiDto docente, int id);

    public ArrayList<DocentiDto> getByPage(int page);

    public int getDocentiRecord();

    public int getDocentiFilterRecord(String filter, String value);

    public ArrayList<DocentiDto> getFilterByPage(String filter, String value, int page);

    public ArrayList<DocentiDto> getByRagioneSociale(String value);

    /**
     * Paginazione docenti interni
     */
    public int[] getDocentiInterniPages();
    public ArrayList<DipendentiDto> getDocentiInterni(int page);

    /**
     * Paginazione filtri docenti interni
     */
    public int[] getDocentiInterniPagesFilterNome(String nome);
    public int[] getDocentiInterniPagesFilterCognome(String cognome);

    public ArrayList<DipendentiDto> filterDocentiInterniByNome(String nome, int page);
    public ArrayList<DipendentiDto> filterDocentiInterniByCognome(String cognome, int page);

    public boolean getRelazioni(int tipo, int id);
}
