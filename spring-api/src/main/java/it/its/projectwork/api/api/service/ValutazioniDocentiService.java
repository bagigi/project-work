package it.its.projectwork.api.api.service;

import it.its.projectwork.api.api.dao.ValutazioniDocentiDao;

import java.util.ArrayList;

public interface ValutazioniDocentiService {
    public void inserisciValutazione(ValutazioniDocentiDao dao);
    public ArrayList<ValutazioniDocentiDao> getValutazioniDocentiInterni(int docenteId, int page);
    public ArrayList<ValutazioniDocentiDao> getValutazioniDocentiEsterni(int docenteId, int page);
    public ValutazioniDocentiDao getByDocenteAndCorso(int docente, int corso);
    public int[] getPagesDocentiAll();
    public int[] getPagesByDocenteId(int docenteId, int tipoDocente);
    public boolean elimina(int id);
}
