package it.its.projectwork.api.api.controller;

import it.its.projectwork.api.api.dto.BaseResponseDto;
import it.its.projectwork.api.api.dto.DipendentiDto;
import it.its.projectwork.api.api.service.DipendentiService;
import org.hibernate.internal.util.xml.BaseXMLEventReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping(value = "api/dipendenti")
public class DipendentiController {
    @Autowired
    DipendentiService dipendentiService;

    @GetMapping(produces = "application/json")
    public BaseResponseDto<ArrayList<DipendentiDto>> getAll(){
        BaseResponseDto<ArrayList<DipendentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DipendentiDto> dipendenti = dipendentiService.getAll();

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dipendenti);

        return responseDto;
    }

    @GetMapping(value = "/{matricola}", produces = "application/json")
    public BaseResponseDto<DipendentiDto> getByMatricola(@PathVariable("matricola") int matricola){
        BaseResponseDto<DipendentiDto> responseDto = new BaseResponseDto<>();

        DipendentiDto dto = this.dipendentiService.getByMatricola(matricola);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @GetMapping(value = "/filter/{value}", produces = "application/json")
    public BaseResponseDto<ArrayList<DipendentiDto>> getFilter(@PathVariable("value") String value){
        BaseResponseDto<ArrayList<DipendentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DipendentiDto> dto = this.dipendentiService.getByNomeCognomeMatricola(value);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    @GetMapping(value = "/id/{id}", produces = "application/json")
    public BaseResponseDto<DipendentiDto> getById(@PathVariable("id") int id){
        BaseResponseDto<DipendentiDto> responseDto = new BaseResponseDto<>();

        DipendentiDto dto = this.dipendentiService.getById(id);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    /**
     * Paginazione
     */
    @GetMapping(value = "/info/pages/all", produces = "application/json")
    public BaseResponseDto<Integer> getPages(){
        BaseResponseDto<Integer> responseDto = new BaseResponseDto<>();

        System.out.println("risposta: " + Arrays.toString(this.dipendentiService.getPages()));

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(this.dipendentiService.getPages());

        return responseDto;
    }

    @GetMapping(value = "/page/{page}", produces = "application/json")
    public BaseResponseDto<ArrayList<DipendentiDto>> getByPage(@PathVariable("page") int page){
        BaseResponseDto<ArrayList<DipendentiDto>> responseDto = new BaseResponseDto<>();

        ArrayList<DipendentiDto> dto = this.dipendentiService.getByPage(page);

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");
        responseDto.setResponse(dto);

        return responseDto;
    }

    /**
     * Paginazione filtri
     */
    @GetMapping(value = "/info/pages", produces = "application/json")
    public BaseResponseDto<int[]> getPagesFilter(
            @RequestParam String filtro,
            @RequestParam String valore
    ){
        BaseResponseDto<int[]> responseDto = new BaseResponseDto<>();

        if(filtro.equals("nome")) {
            System.out.println("nome: " + filtro);
            responseDto.setResponse(this.dipendentiService.pageFilterNome(valore));
        }
        else if(filtro.equals("cognome")) {
            System.out.println("cognome: " + filtro);
            responseDto.setResponse(this.dipendentiService.pageFilterCognome(valore));
        }

        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }

    /**
     * Filtri
     */
    @GetMapping(value = "/filtro", produces = "application/json")
    public BaseResponseDto<ArrayList<DipendentiDto>> filtra(
            @RequestParam String filtro,
            @RequestParam String valore,
            @RequestParam int page
    ){
        BaseResponseDto<ArrayList<DipendentiDto>> responseDto = new BaseResponseDto<>();

        if(filtro.equals("nome")) responseDto.setResponse(this.dipendentiService.filterByNome(valore, page));
        else if(filtro.equals("cognome")) responseDto.setResponse(this.dipendentiService.filterByCognome(valore, page));
        responseDto.setTimestamp(new Date());
        responseDto.setStatus(HttpStatus.OK.value());
        responseDto.setMessage("Servizio elaborato correttamente");

        return responseDto;
    }
}
