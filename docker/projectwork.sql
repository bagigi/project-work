create database project_work_test;
use project_work_test;

create table if not exists docenti(
	 docente_id int(7) auto_increment,
	 cognome varchar(30),
	 nome varchar(30),
	 titolo varchar(30),
	 ragione_sociale varchar(50) not null,
	 indirizzo varchar(35),
	 localita varchar(33),
	 provincia varchar(20),
	 nazione varchar(20),
	 telefono varchar(20),
	 fax varchar(20),
	 partita_iva varchar(20),
	 referente varchar(40),
	 primary key (docente_id)
);

create table if not exists societa (
	societa_id int(7) not null auto_increment primary key,
	ragione_sociale varchar(80) not null,
	indirizzo varchar(80) not null,
	provincia varchar(30) not null,
	nazione varchar(30) not null,
	telefono varchar(20) not null,
	fax varchar(20),
	partita_iva varchar(30) not null
);

create table if not exists dipendenti(
    	dipendente_id int(7) auto_increment,
    	matricola int(32) not null,
    	societa int(32) not null,
    	cognome varchar(30) not null,
    	nome varchar(30) not null,
    	sesso varchar(1) not null,
    	data_nascita Date not null,
    	luogo_nascita varchar(50) not null,
    	stato_civile varchar(20) not null,
    	titolo_studio varchar(35) not null,
    	conseguito_presso varchar(50) not null,
    	anno_conseguimento int(4) not null,
    	tipo_dipendente varchar(30) not null,
    	qualifica varchar(30) not null,
    	livello varchar(20) not null,
    	data_assunzione Date not null,
    	responsabile_risorsa varchar(30) not null,
    	responsabile_area varchar(30) not null,
    	data_fine_rapporto Date not null,
    	data_scadenza_contratto Date not null,
    	primary key (dipendente_id),
    	foreign key (societa) references societa (societa_id)
);

CREATE TABLE if not exists tipi(
    	tipo_id int(7) NOT NULL AUTO_INCREMENT,
    	descrizione varchar(100) DEFAULT NULL,
    	PRIMARY KEY (`tipo_id`)
);
  
CREATE TABLE if not exists tipologie(
	tipologia_id int(7) NOT NULL AUTO_INCREMENT,
	descrizione varchar(40) DEFAULT NULL,
	PRIMARY KEY (`tipologia_id`)
);
  
CREATE TABLE if not exists misure(
    	misura_id int(7) NOT NULL AUTO_INCREMENT,
    	descrizione varchar(40) DEFAULT NULL,
    	PRIMARY KEY (misura_id)
 );

CREATE TABLE if not exists corsi (
    	corso_id int(7) NOT NULL AUTO_INCREMENT,
    	tipo int(32) DEFAULT NULL,
    	tipologia int(32) DEFAULT NULL,
    	descrizione varchar(40) DEFAULT NULL,
    	edizione varchar(40) DEFAULT NULL,
    	previsto int(32) DEFAULT NULL,
 	    erogato int(32) DEFAULT NULL,
    	durata int(32) DEFAULT NULL,
    	misura int(32) DEFAULT NULL,
    	note varchar(60) DEFAULT NULL,
    	luogo varchar(40) DEFAULT NULL,
    	ente int(40) NOT NULL,
    	data_erogazione date DEFAULT NULL,
    	data_chiusura date DEFAULT NULL,
    	data_censimento date DEFAULT NULL,
    	interno int(32) DEFAULT NULL,
    	PRIMARY KEY (corso_id),
    	foreign key (tipo) references tipi (tipo_id),
    	foreign key (tipologia) references tipologie (tipologia_id),
    	foreign key (misura) references misure (misura_id)
);

CREATE TABLE if not exists corsi_docenti (
	corso_docenti_id int(7) NOT NULL AUTO_INCREMENT,
	corso int(32) DEFAULT NULL,
	docente int(32) DEFAULT NULL,
	interno varchar(40) DEFAULT NULL,
	PRIMARY KEY (corso_docenti_id),
	foreign key (corso) references corsi (corso_id)
);

create table if not exists valutazioni_docenti(
    valutazione_docenti_id int(7) auto_increment,
	corso int(15) not null,
	docente int(15) not null,
	completezza int(3) not null,
	chiarezza int(3) not null,
	disponibilita int(3) not null,
	media float(5) not null,
	primary key (valutazione_docenti_id),
	foreign key (corso) references corsi (corso_id)
);

create table if not exists valutazioni_corsi(
	valutazione_corsi_id int(7) auto_increment,
	corso int(15) not null,
	crescita_professionale int(3) not null,
	lavoro_attuale int(3) not null,
	argomenti int(3) not null,
	approfondimento int(3) not null,
	durata int(3) not null,
	divisione_teoria_pratica int(3) not null,
	esercitazioni int(3) not null,
	materiale_didattico int(3) not null,
	responsabile int(3) not null,
	media float(5) not null,
	primary key (valutazione_corsi_id),
	foreign key (corso) references corsi (corso_id)
);

create table if not exists corsi_utenti (
	corsi_utenti_id int(7) not null auto_increment primary key, 
	corso int(32) not null,
	dipendente int(32) not null,
	durata int(32) not null,
	foreign key (corso) references corsi (corso_id),
	foreign key (dipendente) references dipendenti (dipendente_id)
);

create table if not exists valutazione_utenti(
    	valutazione_utenti_id int(32) auto_increment primary key,
    	impegno int(3) not null,
    	logica int(3) not null,
    	apprendimento int(3) not null,
	velocita int(3) not null,
	ordine_precisione int(3) not null,
	responsabile int(3) not null,
    	media float(5) not null,
    	dipendente int(32) not null,
    	corso int(32) not null,
    	foreign key (dipendente) references dipendenti (dipendente_id),
    	foreign key (corso) references corsi (corso_id)
);


INSERT INTO `societa` (`ragione_sociale`, `indirizzo`, `provincia`, `nazione`, `telefono`, `fax`, `partita_iva`) VALUES
('vendiamo asparagi', 'Via Ciccio 69', 'Padova', 'Italia', '2659871403', '258', '123456789'),
('SEC', 'Via Zeta 63', 'Padova', 'Italia', '3245698170', '456', '258963147'),
('Gazprom', 'Via Mare 99', 'Roma', 'Italia', '0925874136', '135', '258852369'),
('Lidl', 'Via Padova 29', 'Milano', 'Italia', '2589631470', '512', '321405689'),
('Aliper', 'Via Porticciolo 199', 'Verona', 'Italia', '0289563478', '546', '002258569'),
('Scottex', 'Via Da Qui 1', 'Treviso', 'Italia', '1023654789', '250', '145963287'),
('BMetal', 'Via San Bonifacio 1', 'Venezia', 'Italia', '0258461937', '689', '545487245'),
('Regina', 'Via Girolametto 25', 'Lucca', 'Italia', '1425352424', '010', '215461325');


INSERT INTO `dipendenti` (`matricola`, `societa`, `cognome`, `nome`, `sesso`, `data_nascita`, `luogo_nascita`, `stato_civile`, `titolo_studio`, `conseguito_presso`, `anno_conseguimento`, `tipo_dipendente`, `qualifica`, `livello`, `data_assunzione`, `responsabile_risorsa`, `responsabile_area`, `data_fine_rapporto`, `data_scadenza_contratto`) VALUES
(101, 1, 'Mozzato', 'Toni', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2021, 'manovale', 'alta', 'alterrimo', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(102, 1, 'Castiglione', 'Geronimo', 'M', '2000-09-13', 'Camposampiero', 'Celibe', 'Diploma', 'Istituto Tecnico', 2019, 'Programmatore', 'Ingegnere', 'base', '2020-03-12', 'no', 'no', '2020-04-05', '2020-04-06'),
(103, 1, 'Arturo', 'Angelo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2009, 'segretario', 'impiegato', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(104, 1, 'Ferrari', 'Enzo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2018, 'stagista', 'alta', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(105, 1, 'Sabbatini', 'Luciano', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2019, 'Programmatore', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(106, 2, 'Pugliesi', 'Colombo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2006, 'manovale', 'capo-ufficio', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(107, 2, 'Manna', 'Gastone', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2003, 'stagista', 'impiegato', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(108, 2, 'De Luca', 'Gustavo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2014, 'manovale', 'alta', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(109, 2, 'Sabbatini', 'Gustavo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2015, 'Programmatore', 'alta', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(110, 2, 'Bianchi', 'Rosario', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2015, 'segretario', 'Ingegnere', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(111, 3, 'Endrizzi', 'Consuelo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2019, 'manovale', 'impiegato', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(112, 3, 'Raimondo', 'Napolitani', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2015, 'manovale', 'capo-ufficio', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(113, 3, 'Bruno', 'Mario', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2014, 'Programmatore', 'alta', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(114, 3, 'Cattaneo', 'Marco', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2013, 'manovale', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(115, 4, 'De Luca', 'Simone', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2016, 'stagista', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(116, 4, 'Milani', 'Raimondo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2003, 'segretario', 'impiegato', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(117, 4, 'Padovesi', 'Albertino', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2017, 'manovale', 'Ingegnere', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(118, 4, 'Zito', 'Armando', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2015, 'manovale', 'alta', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(119, 4, 'Romano', 'Manuel', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2004, 'Programmatore', 'capo-ufficio', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(120, 5, 'Milano', 'Secondo', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2002, 'manovale', 'alta', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(121, 5, 'Cattaneo', 'Mattia', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2015, 'manovale', 'impiegato', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(122, 5, 'Pirozzi', 'Armando', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2013, 'stagista', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(123, 5, 'Marino', 'Filiberto', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2011, 'segretario', 'capo-ufficio', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(124, 5, 'Angelo', 'ristiano', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2012, 'Programmatore', 'alta', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(125, 6, 'Bergamaschi', 'Sebastiano ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2013, 'manovale', 'Ingegnere', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(126, 6, 'Rossi', 'Tiziano ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2005, 'manovale', 'capo-ufficio', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(127, 6, 'Sagese', 'Adamo ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2008, 'stagista', 'impiegato', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(128, 6, 'Bellucci', 'Furio ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2018, 'segretario', 'impiegato', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(129, 6, 'Cocci', 'Cesare ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2019, 'Programmatore', 'capo-ufficio', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(130, 7, 'Rizzo', 'Galeazzo ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2016, 'stagista', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(131, 7, 'Schiavone', 'Ugo ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2004, 'segretario', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(132, 7, 'Lo Duca', 'Tommaso', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2014, 'manovale', 'Ingegnere', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(133, 7, 'Rossi', 'Ruggero ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2013, 'Programmatore', 'impiegato', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(134, 7, 'Barese', 'Ettore ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2012, 'manovale', 'alta', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(135, 8, 'Napolitano', 'Guarino ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2001, 'manovale', 'capo-ufficio', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(136, 8, 'Rizzo', 'Flavio ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2000, 'segretario', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(137, 8, 'Manfrin', 'Emanuele', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2005, 'stagista', 'impiegato', 'mediocre', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(138, 8, 'Udinese', 'Carmine ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2018, 'Programmatore', 'capo-ufficio', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(139, 8, 'Moretti', 'Renato ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2019, 'segretario', 'Ingegnere', 'esperto', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06'),
(140, 8, 'Siciliano', 'Iacopo ', 'M', '2020-04-01', 'Cittadella', 'Sposato', 'Terza media', 'Casa sua', 2007, 'Programmatore', 'Ingegnere', 'base', '2020-04-07', 'no', 'no', '2020-04-05', '2020-04-06');


INSERT INTO `docenti` (`nome`, `cognome`, `titolo`, `ragione_sociale`, `indirizzo`, `localita`, `provincia`,`nazione`,`telefono`,`fax`,`partita_iva`,`referente`) VALUES
('Mauro', 'Verdi', 'Ingegnere', 'Corvallis', 'Via Dotto 10', 'Borgoricco', 'Sassari', 'Italia', '0123697845', '1598746320', '125896', 'Ing. Verdi'),
('Gino', 'Paoli', 'Dottore', 'Corvallis', 'Via Desman 45', 'Verona', 'Verona', 'Italia', '0123697845', '1598746320', '2581404', 'Dott. Paoli'),
('Alberto', 'Fantinato', 'Ragioniere', 'Corvallis', 'Via Europa 52', 'Martellago', 'Vennzia', 'Italia', '542135414', '453564136', '123654', 'Rag. Fantinato'),
('Sandro', 'Coppola', 'Perito', 'Forema', 'Via Graticolato 186', 'Castelfranco', 'Treviso', 'Italia', '435446451256', '453432541.5', '147856', 'Per. Coppola'),
('Gianluca', 'Franciosi', 'Ragioniere', 'Corvallis', 'Via Cal 1', 'Cagliari', 'Cagliari', 'Italia', '5434523454', '354123541353', '585858', 'Rag. Franciosi'),
('Fabrizio', 'Roma', 'Dottore', 'Corvallis', 'Via Nello 158', 'Messina', 'Messina', 'Italia', '435455421335', '2583697410', '102012', 'Dott. Roma'),
('Endy', 'Giallini', 'Ingegnere', 'Corvallis', 'Via Padova 132', 'Lecce', 'Lecce', 'Italia', '4444535613', '4534125434', '145415', 'Ing. Giallini'),
('Francesco', 'Rossi', 'Ragioniere', 'Corvallis', 'Via Italia 35', 'Matera', 'Matera', 'Italia', '365435563', '453464153446', '256526', 'Rag. Rossi'),
('Sandy', 'Cremonini', 'Dottore', 'Forema', 'Via Spagna 86', 'Cormano', 'Milano', 'Italia', '635465136', '4563415344', '589856', 'Dott. Cremonini'),
('Pietro', 'Cesarini', 'Perito', 'Corvallis', 'Via Francia 25', 'Massa', 'Massa', 'Italia', '356143641', '4536445354', '698956', 'Per. Cesarini'),
('Luca', 'Genovese', 'Ingegnere', 'Corvallis', 'Via Polonia 54', 'Camposampiero', 'Padova', 'Italia', '453435643', '453445341', '777889', 'Ing. Genovese'),
('Fabio', 'Volpato', 'Ragioniere', 'Forema', 'Via Giappone 54', 'Rimini', 'Rimini', 'Italia', '45345345345', '2578547854', '867867', 'Rag. Volpato'),
('Carlo', 'Pastrello', 'Dottore', 'Corvallis', 'Via Hokkaido 43', 'Misano', 'Rimini', 'Italia', '1425367890', '325896740', '2581404', 'Dott. Pastrello'),
('Franco', 'Polo', 'Perito', 'Corvallis', 'Via Austria 8', 'Olbia', 'Olbia', 'Italia', '0123697845', '1598746320', '333553', 'Per. Polo'),
('Gianpaolo', 'Andrigo', 'Dottore', 'Corvallis', 'Via Santa 8', 'Scampia', 'Napoli', 'Italia', '0123697845', '1598752036', '585866', 'Dott. Andrigo'),
('Gastone', 'Bianchi', 'Ragioniere', 'Forema', 'Via Della Liberazione 8', 'Pordenone', 'Pordenone', 'Italia', '1472596380', '1598746320', '000145', 'Rag. Bianchi'),
('Giuseppe', 'Fumagalli', 'Dottore', 'Corvallis', 'Via Nuova 128', 'Marghera', 'Venezia', 'Italia', '1235789640', '1598746320', '154520', 'Dott. Fumagalli'),
('Michele', 'Brambilla', 'Perito', 'Corvallis', 'Via Milano 8', 'Montegrotto', 'Padova', 'Italia', '4521036879', '1598746320', '787878', 'Per. Brambilla'),
('Andrea', 'Carrin', 'Ragioniere', 'Corvallis', 'Via Lusore 83', 'Livorno', 'Livorno', 'Italia', '0125896347', '1598746320', '555555', 'Rag. Carrin'),
('Gabriele', 'Boscolo', 'Dottore', 'Forema', 'Via Napoli 82', 'Pinzolo', 'Trento', 'Italia', '1425368790', '1598746320', '363633', 'Dott. Boscolo');

INSERT INTO tipi (tipo_id, descrizione) VALUES 
(1,'Java'),
(2,'Internet'),
(3,'Client-Server'),
(4,'Programmazione Mainframe'),
(5,'RPG'),
(6,'Cobol'),
(7,'Programmazione di Sistemi EDP'),
(8,'Object-Oriented'),
(9,'Sistema di Gestione Qualità'),
(10,'Privacy'),
(11,'Analisi e Programmazione'),
(12,'Programmazione PC'),
(13,'Project Management'),
(14,'Programmazione'),
(15,'CICS'),
(16,'DB2'),
(17,'Linguaggio C'),
(18,'Data Base'),
(19,'Introduzione all''Informatica'),
(20,'Office'),
(21,'JCL'),
(22,'Marketing'),
(23,'Oracle'),
(24,'Formazione Sicurezza (626/94; 81/2008)'),
(25,'Inglese'),
(26,'Amministrazione/Finanza'),
(27,'Windows'),
(28,'Word'),
(29,'Office'),
(30,'Excel'),
(31,'Easytrieve'),
(32,'Comunicazione'),
(33,'Access'),
(34,'Formazione per Formatori'),
(35,'Tecnica Bancaria'),
(36,'Lotus Notes'),
(37,'Visualage'),
(38,'Websphere'),
(39,'Reti/Elettronica'),
(40,'SAS'),
(41,'UML'),
(42,'ViaSoft'),
(43,'Visual Basic'),
(44,'Sistemi Informativi'),
(45,'Cartografia Vettoriale'),
(46,'Cartografia Raster'),
(47,'Gestione Elettronica Documenti-Modulistica'),
(48,'Territorio e Ambiente'),
(49,'Sistemi Informativi Territoriali'),
(50,'Editoria'),
(51,'VARIE'),
(52,'AS/400'),
(53,'Gestione Progetti'),
(55,'Organizzazione'),
(56,'Funzionale Normativo'),
(57,'Funzionale'),
(58,'Manageriale'),
(60,'Metodologia'),
(61,'Prodotti'),
(62,'Business Object'),
(63,'Sistemi di Business Intelligence'),
(64,'Formazione Apprendisti'),
(66,'Primavera'),
(67,'XML'),
(68,'Cattolica – Servizi in ambito Sinistri'),
(69,'Ambito Assicurativo'),
(70,'Gestione della Sicurezza delle Informazioni (SGSI)'),
(71,'PHP'),
(72,'CMS/E-commerce'),
(73,'Microsoft Azure'),
(75,'Eurotech IoT'),
(79,'Infrastrutture'),
(80,'Infrastrutture'),
(81,'Android'),
(82,'SQL'),
(83,'Risorse Umane'),
(74,'Middleware - Sistemi Distribuiti'),
(76,'Blockchain'),
(77,'Angular'),
(78,'Soft Skill');

INSERT INTO tipologie (tipologia_id, descrizione) VALUES 
(1,'Ingresso'),
(2,'Aggiornamento'),
(3,'Seminario');

INSERT INTO misure (misura_id, descrizione) VALUES 
(1,'Ore'),
(2,'Giorni');

insert into `corsi` (`tipo`,`tipologia`,`descrizione`,`edizione`,`previsto`,`erogato`,`durata`,`misura`,`note`,`luogo`,`ente`,`data_erogazione`,`data_chiusura`,`data_censimento`,`interno`) VALUES 
(2, 3, 'descrizione 1', 'Prima', 0, 1, 11, 1, 'note 1', 'Rovigo', 1, '2020-01-14', '2020-01-30', '2020-01-25', 1),
(3, 3, 'descrizione 2', 'Seconda', 0, 1, 10, 1, 'note 2', 'Padova', 2, '2020-02-14', '2020-04-30', '2020-02-14', 1),
(4, 3, 'descrizione 3', 'Terza', 0, 1, 1, 1, 'note 3', 'Brindisi', 3, '2020-03-14', '2020-05-30', '2020-03-25', 1),
(5, 3, 'descrizione 4', 'Quarta', 0, 1, 2, 1, 'note 4', 'Verona', 4, '2020-04-14', '2020-06-30', '2020-04-02', 1),
(6, 3, 'descrizione 5', 'Prima', 0, 1, 20, 1, 'note 5', 'Brindisi', 5, '2020-05-14', '2020-07-30', '2020-05-04', 1),
(7, 3, 'descrizione 6', 'Seconda', 0, 1, 2, 1, 'note 6', 'Lecce', 6, '2020-06-14', '2020-08-30', '2020-06-25', 1),
(56, 3, 'descrizione 7', 'Terza', 0, 1, 5, 1, 'note 7', 'Padova', 7, '2020-07-14', '2020-09-30', '2020-07-24', 1),
(74, 3, 'descrizione 8', 'Quarta', 0, 1, 7, 1, 'note 8', 'Lecce', 8, '2020-08-14', '2020-10-30', '2020-08-12', 1),
(34, 2, 'descrizione 9', 'Prima', 0, 1, 14, 1, 'note 9', 'Rovigo', 9, '2020-09-14', '2020-11-30', '2020-09-25', 1),
(55, 2, 'descrizione 10', 'Seconda', 0, 1, 11, 1, 'note 10', 'Rovigo', 11, '2020-10-14', '2020-12-30', '2020-10-01', 1),
(10, 2, 'descrizione 11', 'Terza', 1, 0, 14, 1, 'note 11', 'Lecce', 12, '2020-11-14', '2020-01-30', '2020-11-25', 1),
(12, 2, 'descrizione 12', 'Quarta', 1, 0, 5, 2, 'note 12', 'Brindisi', 13, '2020-12-14', '2020-02-10', '2020-12-17', 1),
(3, 2, 'descrizione 13', 'Prima', 1, 0, 7, 2, 'note 13', 'Verona', 14, '2020-01-14', '2020-03-30', '2020-01-25', 1),
(4, 2, 'descrizione 14', 'Seconda', 1, 0, 30, 2, 'note 14', 'Padova', 15, '2020-02-14', '2020-04-30', '2020-02-14', 1),
(58, 2, 'descrizione 15', 'Terza', 1, 0, 11, 2, 'note 15', 'Brindisi', 16, '2020-03-14', '2020-05-30', '2020-03-30', 1),
(64, 1, 'descrizione 16', 'Quarta', 1, 0, 21, 2, 'note 16', 'Padova', 17, '2020-04-14', '2020-06-30', '2020-04-25', 1),
(25, 1, 'descrizione 17', 'Prima', 1, 0, 25, 2, 'note 17', 'Rovigo', 18, '2020-05-14', '2020-07-30', '2020-05-17', 1),
(36, 1, 'descrizione 18', 'Seconda', 1,0, 11, 2, 'note 18', 'Brindisi', 19, '2020-06-14', '2020-08-30', '2020-06-25', 1),
(70, 1, 'descrizione 19', 'Terza', 1, 0, 10, 2, 'note 19', 'Verona', 20, '2020-07-14', '2020-09-30', '2020-07-31', 1),
(62, 1, 'descrizione 20', 'Quarta', 1, 0, 14, 2, 'note 20', 'Lecce', 21, '2020-08-14', '2020-10-30', '2020-08-28', 1);


insert into `corsi_docenti` (`corso`,`docente`,`interno`) VALUES
(1, 20, 1),
(2, 19, 1),
(3, 18, 1),
(4, 17, 1),
(5, 16, 1),
(6, 15, 1),
(7, 14, 1),
(8, 13, 1),
(9, 12, 1),
(10, 11, 1),
(11, 10, 1),
(12, 9, 1),
(13, 8, 1),
(14, 7, 1),
(15, 6, 1),
(16, 5, 1),
(17, 4, 1),
(18, 3, 1),
(19, 2, 1),
(20, 1, 1);


insert into `corsi_utenti` (`corso`,`dipendente`,`durata`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(1, 5, 1),
(2, 6, 1),
(2, 7, 1),
(2, 8, 1),
(2, 9, 1),
(2, 10, 1),
(3, 11, 1),
(3, 12, 1),
(3, 13, 1),
(3, 14, 1),
(3, 15, 1),
(9, 16, 1),
(9, 17, 1),
(9, 18, 1),
(9, 19, 1),
(9, 20, 1),
(10, 21, 1),
(10, 22, 1),
(10, 23, 1),
(10, 24, 1),
(10, 25, 1),
(11, 26, 1),
(11, 27, 1),
(11, 28, 1),
(11, 29, 1),
(11, 30, 1),
(16, 31, 1),
(16, 32, 1),
(16, 33, 1),
(16, 34, 1),
(16, 35, 1),
(17, 36, 1),
(17, 37, 1),
(17, 38, 1),
(17, 39, 1),
(17, 40, 1);


insert into valutazione_utenti (impegno, logica, apprendimento, velocita, ordine_precisione, responsabile, media, dipendente, corso) VALUES
(3,3,3,3,3,3,3,1,1),
(1,1,1,1,1,1,1,2,1),
(5,5,5,5,5,5,5,3,1),
(4,4,4,4,4,4,4,4,1),
(2,2,2,2,2,2,2,5,1),
(3,3,3,3,3,3,3,16,9),
(1,1,1,1,1,1,1,17,9),
(5,5,5,5,5,5,5,18,9),
(4,4,4,4,4,4,4,19,9),
(2,2,2,2,2,2,2,20,9),
(3,3,3,3,3,3,3,31,16),
(1,1,1,1,1,1,1,32,16),
(5,5,5,5,5,5,5,33,16),
(4,4,4,4,4,4,4,34,16),
(2,2,2,2,2,2,2,35,16);


insert into valutazioni_docenti (corso, docente, completezza, chiarezza, disponibilita, media) VALUES
(1,20,1,3,4,3),
(2,19,1,1,1,3),
(9,12,1,3,4,3),
(10,11,1,1,1,3),
(16,5,1,3,4,3),
(17,4,1,1,1,3);

insert into valutazioni_corsi (corso, crescita_professionale , lavoro_attuale , argomenti , approfondimento , durata , divisione_teoria_pratica , esercitazioni , materiale_didattico , responsabile , media) VALUES
(1,1,2,3,4,5,4,3,2,2,3),
(2,1,1,1,1,1,1,1,1,2,1),
(9,1,2,3,4,5,4,3,2,2,3),
(10,1,1,1,1,1,1,1,1,2,1),
(16,1,2,3,4,5,4,3,2,2,3),
(17,1,1,1,1,1,1,1,1,2,1);

