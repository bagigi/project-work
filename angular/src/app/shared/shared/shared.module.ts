import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabellaComponent } from "../tabella/tabella.component";
import { FilterComponent } from "../filtra/filtra.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatToolbarModule } from "@angular/material/toolbar";
import { PageSelectorComponent } from "../page-selector/page-selector.component";
import { InfoUserComponent } from "../info-user/info-user.component";
import { AlertDialogComponent } from "../alert-dialog/alert-dialog.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";

@NgModule({
  declarations: [
    TabellaComponent,
    FilterComponent,
    PageSelectorComponent,
    InfoUserComponent,
    AlertDialogComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [
    TabellaComponent,
    FilterComponent,
    PageSelectorComponent,
    InfoUserComponent,
    AlertDialogComponent,
  ],
})
export class SharedModule {}
