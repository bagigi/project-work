import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DipendentiService } from '../../../core/service/dipendenti.service';
import { AlunniService } from '../../../core/service/alunni.service';

export interface DialogData {
  idCorso: any;
}

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.css']
})
export class AddDialogComponent implements OnInit {

  public dipendente;
  public items: any[] = [];
  public test: any[] = [1, 0, 1, 1, 0];
  public listaDipendenti: any[] = [];
  public presenti: any[] = [];

  public funzia = false;

  constructor(
    public dialogRef: MatDialogRef<AddDialogComponent>,
    public dipendentiService: DipendentiService,
    public alunniService: AlunniService,
    @Inject(MAT_DIALOG_DATA) public data: number
  ) { }

  ngOnInit(): void {
    this.alunniService.getByCorsoId(this.data).subscribe(res => {
      console.log("alunni gia selezionati: ", res.response);
      res.response.forEach(element => {
        this.presenti.push(element.dipendente.id);
      });
    });

    console.log("array test: ", this.test);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cerca() {
    if (this.dipendente == null) return;
    if (this.dipendente.length >= 3) {
      this.items = [];
      this.dipendentiService.getFilter(this.dipendente).subscribe(res => {
        let arrayDip = res.response;
        arrayDip.forEach(element => {
          if (!this.presenti.includes(element.id)) {
            console.log("dip id: ", element.id)
            /* if (this.ceccati[element.id] == undefined) this.ceccati.splice(element.id, 0, 0);
            else this.ceccati.splice(element.id, 0, 1); */
            if (this.listaDipendenti.includes(element.id)) element.check = true;
            else element.check = false;
            this.items.push(element);
          }
        });
        console.log("items: ", this.items)
      });
    }

    //console.log("TEST: ", this.ceccati[100]);
  }

  ciccioBello() {
    console.log("ciccio: ", this.listaDipendenti);
  }

  addRemove(event: any, id: any) {
    console.log("event: ", event);
    console.log("id: ", id)
    if (event.checked) this.listaDipendenti.push(id);
    else {
      this.listaDipendenti.splice(this.listaDipendenti.indexOf(id), 1);
    }
    console.log("dip: ", this.listaDipendenti);
  }

}
