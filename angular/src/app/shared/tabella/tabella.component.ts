import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TabellaOptions } from "../../api/api-options";

@Component({
  selector: "tabella",
  templateUrl: "./tabella.component.html",
  styleUrls: ["./tabella.component.css"],
})
export class TabellaComponent implements OnInit {
  @Input() lista: any[];
  @Input() public options: TabellaOptions;
  @Input() public selectiontype: string;
  @Input() delete: boolean;
  @Input() edit: boolean;
  @Input() info: boolean;
  @Input() voto: boolean;

  @Output() selectRows: EventEmitter<any> = new EventEmitter<any>();
  @Output() onEditEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDeleteEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() onInfoEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() onVotoEvent: EventEmitter<any> = new EventEmitter<any>();

  selected: boolean[] = [];

  currentIndex: number;

  select(index: number) {
    if (this.selectiontype === "multiple" || this.selectiontype === "single") {
      if (this.selectiontype === "single") {
        let b = this.selected[index];
        this.selected = [];
        this.selected[index] = b;
      }
      this.selected[index] = !this.selected[index];
      const listaSelect = [];
      this.selected.forEach((item, i) => {
        if (item) {
          listaSelect.push(this.lista[i]);
        }
      });
      this.selectRows.emit(listaSelect);
    }
  }

  constructor() { }

  ngOnInit() {
    console.log("lista: ", this.lista);
  }

  onDelete(item) {
    this.onDeleteEvent.emit(item.id);
  }
  onEdit(item) {
    console.log(item);
    this.onEditEvent.emit(item.id);
  }
  onInfo(item) {
    this.onInfoEvent.emit(item.id);
  }
  onVoto(item) {
    this.onVotoEvent.emit(item.id);
  }
}
