import { Component, OnInit, Inject, Output, EventEmitter } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { FormGroup, FormBuilder } from "@angular/forms";

export class DialogData {
  title: String;
  options: any[];
}

@Component({
  selector: "app-dialog",
  templateUrl: "./dialog.component.html",
  styleUrls: ["./dialog.component.css"],
})
export class DialogComponent implements OnInit {
  @Output()
  newUser: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
    console.log(data)
  }

  ngOnInit(): void { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
