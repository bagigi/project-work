import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-page-selector",
  templateUrl: "./page-selector.component.html",
  styleUrls: ["./page-selector.component.css"],
})
export class PageSelectorComponent implements OnInit {
  @Input() public pages: number;
  public paginaora : number;
  public arraypag: number
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();
  public i:number;
  public selected: boolean[] = [];

  constructor() {
    console.log("pages: ", this.pages);

  }

  ngOnInit(): void {
    this.selected[0] = true;
    /* for (let k = 1; k != this.pages; k++) {
      this.selected[k] = false;
    } */
    
    this.arraypag=this.pages;
    console.log("selezionato: ", this.selected);
    this.i=0;
    while(this.arraypag[this.i]!=null){
      this.i++

    }


    console.log("PAGINEEEEEEEEEEEEEEEEE: ", this.i);
    this.paginaora=1;
  }

  ciccio(type: any, event: any) {
    console.log("event: ", event);
    this.selected = [];
    this.selected[event - 1] = true;
    console.log("selected: ", this.selected)
    this.onClick.emit(event);

    this.paginaora=event;
    console.log("paginaora", this.paginaora)
  }

  cicciosucc(){

    if(this.paginaora<this.i){
    console.log("event: ", event);
    this.selected = [];
    this.selected[this.paginaora] = true;
    console.log("selected: ", this.selected)
    this.onClick.emit(this.paginaora+1);

    this.paginaora=this.paginaora+1;
    console.log("paginaora", this.paginaora)
    }
    
  }
  cicciopre(){
    if(this.paginaora>1){
    this.selected = [];
    this.selected[this.paginaora-2] = true;
    console.log("selected: ", this.selected)
    this.onClick.emit(this.paginaora-1);
    this.paginaora=this.paginaora-1
    console.log("paginaorapre: " , this.paginaora)
    }
  }
}
