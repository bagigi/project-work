import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-filter",
  templateUrl: "./filtra.component.html",
  styleUrls: ["./filtra.component.css"],
})
export class FilterComponent implements OnInit {
  public formgroup: FormGroup;
  @Output() onFilter: EventEmitter<any> = new EventEmitter<any>();
  @Input() colsOption: any;

  public selezione: any = "nome";
  public value: any;

  constructor(public fb: FormBuilder) {}

  ngOnInit() {
    this.formgroup = this.fb.group({
      key: [this.colsOption[0].name],
      filter: [""],
    });
  }

  filtra() {
    let awg: any[] = [];
    awg[0] = this.selezione;
    awg[1] = this.value;
    this.onFilter.emit(awg);
  }

  seleziona(event: any) {
    this.selezione = event;
  }

  azzera(event) {
    console.log("input: ", event);
    if (event.length == 0) {
      this.onFilter.emit(-1);
    }
  }
}
