import { Component, OnInit, Input } from "@angular/core";
import { TabellaOptions } from "../../api/api-options";

@Component({
  selector: "app-info-user",
  templateUrl: "./info-user.component.html",
  styleUrls: ["./info-user.component.css"],
})
export class InfoUserComponent implements OnInit {
  @Input() listaAlunni: any;
  @Input() listaCorsi: any;
  @Input() listaDocenti: any;

  @Input() pageDocenti: any;
  @Input() pageAlunni: any;
  @Input() pageCorsi: any;

  @Input() alunno: boolean;
  @Input() docente: boolean;
  @Input() corso: boolean;

  @Input() tableOptions: TabellaOptions;

  constructor() {}

  ngOnInit(): void {}

  onEdit(event: any) {
    console.log(event);
  }
  onDelete(event: any) {
    console.log(event);
  }
}
