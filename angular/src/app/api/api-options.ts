export interface ColOption {
    label: string;
    name: string;
  }
  
  export interface TabellaOptions {
    colsOptions: ColOption[];
  }