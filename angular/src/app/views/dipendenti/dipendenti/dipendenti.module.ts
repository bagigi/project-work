import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DipendentiRoutingModule } from './dipendenti-routing.module';
import { DipendentiComponent } from './dipendenti.component';
import { TabellaComponent } from '../../../shared/tabella/tabella.component';
import { FilterComponent } from '../../../shared/filtra/filtra.component';
import { PageSelectorComponent } from '../../../shared/page-selector/page-selector.component';


@NgModule({
  declarations: [
    DipendentiComponent, 
    TabellaComponent, 
    FilterComponent,
    PageSelectorComponent],
  imports: [
    CommonModule,
    DipendentiRoutingModule,
  ]
})
export class DipendentiModule { }
