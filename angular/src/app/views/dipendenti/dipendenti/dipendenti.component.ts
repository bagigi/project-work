import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../../../../core/service/api.service";
import { DipendentiService } from "../../../../core/service/dipendenti.service";
import { TabellaOptions } from "../../../api/api-options";

@Component({
  selector: "app-dipendenti",
  templateUrl: "./dipendenti.component.html",
  styleUrls: ["./dipendenti.component.css"],
})
export class DipendentiComponent implements OnInit {
  public path = "api/dipendenti/delete";

  options: TabellaOptions = {
    colsOptions: [
      {
        label: "Nome",
        name: "nome",
      },
      {
        label: "Cognome",
        name: "cognome",
      },
      {
        label: "Livello",
        name: "livello",
      },
      {
        label: "Tipo dipendente",
        name: "tipo_dipendente",
      },
    ],
  };
  public lista: any[];
  public copy: any[];
  public pages: any[];
  public filtro: any[];
  public keys = [
    {
      label: "Nome",
      name: "nome",
    },
    {
      label: "Cognome",
      name: "cognome",
    },
  ];
  constructor(
    public routeActive: ActivatedRoute,
    public dipendentiService: DipendentiService,
    public router: Router,
    public api: ApiService
  ) {}

  ngOnInit() {
    this.dipendentiService.getByPage(1).subscribe((res) => {
      this.lista = res.response;
      this.copy = res.response;
    });

    this.dipendentiService.getPages().subscribe((res) => {
      this.pages = res.response;
      console.log("Numero di pagine: ", res.response);
    });
  }

  select(input: any[]) {
    const sogg = input[0];
    this.router.navigate(["dipendenti", sogg.docente_id]);
  }
  onDeleteHandler(id: any) {
    this.api.delete(this.path, id).subscribe((r) => {
      this.api.get("api/dipendenti/").subscribe((res) => {
        this.lista = res.response;
      });
    });
    /* this.dipendenteService.deleteById(id).subscribe(r => {
      this.dipendenteService.getAll().subscribe(res => {
        this.lista = res;
      });
    }); */
  }

  onInfoHandler(id: any) {
    this.router.navigate(["dipendenti/info", id]);
  }

  onEditHandler(id: any) {
    console.log(id);
    this.router.navigate(["dipendenti/info", id]);
  }

  filter(res: any) {
    if (res == -1) {
      this.dipendentiService.getByPage(1).subscribe((res) => {
        this.lista = res.response;
        this.copy = res.response;
        this.dipendentiService
          .getPages()
          .subscribe((res) => (this.pages = res.response));
      });

      this.filtro = null;
    } else {
      this.filtro = res;

      let percorsoPages: String =
        "/info/pages?filtro=" + res[0] + "&valore=" + res[1];
      let percorsoFilter: String =
        "/filtro?filtro=" + res[0] + "&valore=" + res[1] + "&page=1";

      this.dipendentiService.getFilterPages(percorsoPages).subscribe((res) => {
        this.pages = res.response;
        this.dipendentiService
          .getFilterList(percorsoFilter)
          .subscribe((res) => {
            this.lista = res.response;
          });
      });
    }
  }

  aggiorna(event) {
    if (this.filter[0] != null) {
      let percorsoFilter: String =
        "/info/pages?filter=" + this.filter[0] + "&value=" + this.filter[1];
      this.dipendentiService
        .getFilterList(percorsoFilter + "&page=" + event)
        .subscribe((res) => {
          this.lista = res.response;
          console.log("lista: ", this.lista);
        });
    } else {
      this.dipendentiService.getByPage(event).subscribe((res) => {
        this.lista = res.response;
        this.copy = res.response;
        console.log("lista2: ", this.lista);
        console.log("copy: ", this.copy);
      });
    }
  }
}
