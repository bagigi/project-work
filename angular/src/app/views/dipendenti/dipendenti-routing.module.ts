import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DipendentiComponent } from './dipendenti/dipendenti.component';
import { InfoDipendentiComponent } from './info-dipendenti/info-dipendenti.component';


const routes: Routes = [
  {
    path: "",
    component: DipendentiComponent,
    data: {
      title: "Dipendenti"
    }
  },
  {
    path: "info/:id",
    component: InfoDipendentiComponent,
    data: {
      title: "Info Dipendente",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DipendentiRoutingModule { }
