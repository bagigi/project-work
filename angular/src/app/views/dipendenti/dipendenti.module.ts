import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DipendentiRoutingModule } from "./dipendenti-routing.module";
import { DipendentiComponent } from "./dipendenti/dipendenti.component";
import { SharedModule } from "../../shared/shared/shared.module";
import { InfoDipendentiComponent } from "./info-dipendenti/info-dipendenti.component";
import { FormGroup, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { PageSelectorComponent } from "../../shared/page-selector/page-selector.component";

@NgModule({
  declarations: [DipendentiComponent, InfoDipendentiComponent],
  imports: [
    CommonModule,
    DipendentiRoutingModule,
    SharedModule,
    FormsModule,
    MatFormFieldModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
})
export class DipendentiModule {}
