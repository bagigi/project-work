import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DipendentiService } from "../../../../core/service/dipendenti.service";
import { TabellaOptions } from "../../../api/api-options";
import { ValutazioniService } from "../../../../core/service/valutazioni.service";
import { CorsiService } from "../../../../core/service/corsi.service";
import { OptionsService } from "./options.service";
import { element } from "protractor";

@Component({
  selector: "app-info-dipendenti",
  templateUrl: "./info-dipendenti.component.html",
  styleUrls: ["./info-dipendenti.component.css"],
})
export class InfoDipendentiComponent implements OnInit {
  public formgroup: FormGroup;
  public matricola;
  public listaAlunni: any[] = [];
  public listaDocenti: any[] = [];
  public listaCorsi: any[] = [];
  public copy: any[];
  public id;

  public isDocente: boolean = true;
  public hasCorsi: boolean = true;

  public pageCorsi: any[];
  public pageDocenti: any[];
  public pageAlunni: any[];

  public tableOptions: TabellaOptions;

  constructor(
    public fb: FormBuilder,
    public dipendenti: DipendentiService,
    public router: Router,
    public routeActive: ActivatedRoute,
    public voti: ValutazioniService,
    public corsiService: CorsiService,
    public options: OptionsService
  ) {
    this.tableOptions = options.options;
  }

  ngOnInit(): void {
    this.id = this.routeActive.snapshot.params.id;
    this.voti.GetValutazioniDip(this.id, 1).subscribe((res2) => {
      this.copy = res2.response;
      this.listaAlunni = res2.response;

      this.listaAlunni.forEach((element) => {
        this.corsiService.getById(element.corso).subscribe((res) => {
          element.descrizione = res.response.descrizione;
          element.data_chiusura = res.response.data_chiusura;
          element.tipo = res.response.tipo;
        });
      });
    });

    this.voti.GetValutazioniDocente(this.id, "interno", 1).subscribe((res) => {
      if (res.response.length == 0) this.isDocente = false;
      else this.isDocente = true;

      this.listaDocenti = res.response;

      this.voti
        .getPagesValutazioniByDocente(this.id, 0)
        .subscribe((res) => (this.pageDocenti = res.response));

      this.listaDocenti.forEach((element) => {
        this.corsiService.getById(element.corso).subscribe((res) => {
          element.descrizione = res.response.descrizione;
          element.data_chiusura = res.response.data_chiusura;
          element.tipo = res.response.tipo;
        });
      });
    });

    this.corsiService.getCorsiInCorso(this.id, 1).subscribe((res) => {
      console.log("dio bestia: ", res.response);
      this.listaCorsi = res.response;
      this.corsiService
        .getPagesCorsiInCorso(this.id)
        .subscribe((res) => (this.pageCorsi = res.response));

      this.listaCorsi.forEach(
        (element) => (element.media = "Non ancora disponibile")
      );
    });

    this.formgroup = this.fb.group({
      matricola: [""],
      societa: [""],
      cognome: [""],
      nome: [""],
      sesso: [""],
      livello: [""],
      qualifica: [""],
      tipo_dipendente: [""],
    });

    this.matricola = this.routeActive.snapshot.params.id;
    console.log("matricola:", this.matricola);
    this.dipendenti.getByMatricola(this.matricola).subscribe((res) => {
      if (res.response == null) {
        this.dipendenti.getById(this.matricola).subscribe((res) => {
          this.setFormValue(res);
        });
      } else {
        this.setFormValue(res);
      }
    });
  }

  setFormValue(res: any) {
    this.formgroup.controls.matricola.setValue(res.response.matricola);
    this.formgroup.controls.societa.setValue(res.response.societa);
    this.formgroup.controls.cognome.setValue(res.response.cognome);
    this.formgroup.controls.nome.setValue(res.response.nome);
    this.formgroup.controls.sesso.setValue(res.response.sesso);
    this.formgroup.controls.livello.setValue(res.response.livello);
    this.formgroup.controls.qualifica.setValue(res.response.qualifica);
    this.formgroup.controls.tipo_dipendente.setValue(
      res.response.tipo_dipendente
    );
  }
}
