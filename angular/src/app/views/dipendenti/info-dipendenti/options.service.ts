import { Injectable } from "@angular/core";
import { TabellaOptions } from "../../../api/api-options";

@Injectable({
  providedIn: "root",
})
export class OptionsService {
  constructor() {}

  options: TabellaOptions = {
    colsOptions: [
      {
        label: "Nome Corso",
        name: "descrizione",
      },
      {
        label: "Argomento",
        name: "tipo",
      },
      {
        label: "Data di Chiusura",
        name: "data_chiusura",
      },
      {
        label: "Media Voto",
        name: "media",
      },
    ],
  };
}
