import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoDipendentiComponent } from './info-dipendenti.component';

describe('InfoDipendentiComponent', () => {
  let component: InfoDipendentiComponent;
  let fixture: ComponentFixture<InfoDipendentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoDipendentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoDipendentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
