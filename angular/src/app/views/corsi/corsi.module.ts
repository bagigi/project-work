import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CorsiRoutingModule } from "./corsi-routing.module";
import { CorsiComponent } from "./corsi/corsi.component";
import { NewCorsoComponent } from "./new-corso/new-corso.component";
import { MatRadioModule } from "@angular/material/radio";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared/shared.module";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatFormField, MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule, MatOptionModule } from "@angular/material/core";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import { MatDialogModule } from "@angular/material/dialog";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { EditCorsoComponent } from "./edit-corso/edit-corso.component";
import { CdkTreeModule } from "@angular/cdk/tree";
import { CdkStepperModule } from "@angular/cdk/stepper";
import { CdkTableModule } from "@angular/cdk/table";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { AddDialogComponent } from "../../shared/add-dialog/add-dialog.component";
import { AlertDialogComponent } from "../../shared/alert-dialog/alert-dialog.component";

@NgModule({
  declarations: [
    CorsiComponent,
    NewCorsoComponent,
    EditCorsoComponent,
    AddDialogComponent,
  ],
  imports: [
    CommonModule,
    CorsiRoutingModule,
    MatRadioModule,
    FormsModule,
    SharedModule,
    MatSlideToggleModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    MatFormFieldModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    CdkTreeModule,
    CdkStepperModule,
    CdkTableModule,
    ScrollingModule,
    MatDialogModule,
  ],
  entryComponents: [AddDialogComponent, AlertDialogComponent],
})
export class CorsiModule {}
