import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CorsiComponent } from "./corsi/corsi.component";
import { NewCorsoComponent } from "./new-corso/new-corso.component";
import { EditCorsoComponent } from './edit-corso/edit-corso.component';

const routes: Routes = [
  {
    path: "tutto",
    component: CorsiComponent,
    data: {
      title: "Corsi",
    },
  },
  {
    path: "nuovo",
    component: NewCorsoComponent,
    data: {
      title: "Nuovo Corso",
    },
  },
  {
    path: "info/:id",
    component: EditCorsoComponent,
    data: {
      title: "Visualizza Corso",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorsiRoutingModule { }
