import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CorsiService } from "../../../../core/service/corsi.service";
import { ActivatedRoute, Router } from "@angular/router";
import { OptionsService } from "./options.service";
import { TipiService } from "../../../../core/service/tipi.service";
import { DocentiService } from "../../../../core/service/docenti.service";
import { DipendentiService } from "../../../../core/service/dipendenti.service";
import { AlunniService } from "../../../../core/service/alunni.service";
import { MatDialog } from "@angular/material/dialog";
import { AddDialogComponent } from "../../../shared/add-dialog/add-dialog.component";
import { ValutazioniService } from "../../../../core/service/valutazioni.service";

@Component({
  selector: "app-edit-corso",
  templateUrl: "./edit-corso.component.html",
  styleUrls: ["./edit-corso.component.css"],
})
export class EditCorsoComponent implements OnInit {
  @ViewChild("selettoreAlunni") selettoreAlunni;

  public formgroup: FormGroup;
  public soggetto;
  public id;

  public listaTipi: any;
  public listaDocenti: any;
  public listaDipendenti: any;
  public alunniSelezionati: any[];

  public tipologia;
  public stato;
  public durata;
  public misura;
  public luogo;
  public edizione;
  public argomento;
  public dataI;
  public dataF;
  public docente;
  public docenteId;
  public tipoDocente;
  public descrizione;
  public interno;
  public note;
  public alunni;
  public tipiSelection;
  public misuraSelection;

  public mediacres;
  public medialav;
  public mediaarg;
  public mediaapp;
  public mediadur;
  public mediadiv;
  public mediaese;
  public mediamat;
  public mediacor;

  public item: any[] = [];

  public tableOptions: any;

  public items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor(
    public fb: FormBuilder,
    public corso: CorsiService,
    public routeActive: ActivatedRoute,
    public router: Router,
    public options: OptionsService,
    public tipiService: TipiService,
    public corsiService: CorsiService,
    public docentiService: DocentiService,
    public dipendentiService: DipendentiService,
    public alunniService: AlunniService,
    public valutazioniService: ValutazioniService,
    public dialog: MatDialog
  ) {
    this.tableOptions = this.options.tableOptions;
  }

  ngOnInit(): void {
    this.formgroup = this.fb.group({
      tipologia: [""],
      stato: [""],
      durata: [""],
      luogo: [""],
      edizione: [""],
      argomento: [""],
      dataI: [""],
      dataF: [""],
      tipoDocente: [""],
      descrizione: [""],
      interno: [""],
      note: [""],
      alunni: [""],
    });

    this.id = this.routeActive.snapshot.params.id;
    this.corso.getById(this.id).subscribe((res) => {
      this.tipiService
        .getAll()
        .subscribe((res) => (this.listaTipi = res.response));

      console.log("response: ", res.response);

      this.tipologia = res.response.tipologia;
      if (this.tipologia == "Seminario") this.tipologia = "3";
      if (this.tipologia == "Aggiornamento") this.tipologia = "2";
      if (this.tipologia == "Ingresso") this.tipologia = "1";
      this.stato = res.response.erogato;
      this.durata = res.response.durata;
      if (res.response.misura == "Ore") {
        this.misura = 0;
        this.misuraSelection = 1;
      } else {
        this.misura = 1;
        this.misuraSelection = 2;
      }
      this.luogo = res.response.luogo;
      this.edizione = res.response.edizione;
      this.argomento = res.response.tipo;
      this.dataI = res.response.data_erogazione;
      this.dataF = res.response.data_chiusura;
      this.docente = res.response.ente;
      this.corsiService
        .getDocenteId(this.id)
        .subscribe((res) => (this.docenteId = res.response));
      this.corsiService
        .getArgomento(this.id)
        .subscribe((res) => (this.tipiSelection = res.response));
      this.corsiService.getTipoDocente(this.id).subscribe((res) => {
        if (res.response == 1) this.tipoDocente = "1";
        else this.tipoDocente = "0";
      });
      this.descrizione = res.response.descrizione;
      this.note = res.response.note;
      this.interno = res.response.interno;
      this.alunniService.getByCorsoId(this.id).subscribe((res) => {
        console.log("alunni: ", res.response);
        res.response.forEach((element) => {
          this.item.push(element.dipendente);
        });

        console.log("test: ", this.item);
      });
      //console.log("alunni: ", this.alunni);
      //this.tipoDocente = res.response

      this.formgroup.controls.tipologia.setValue(res.response.tipologia);
      this.formgroup.controls.stato.setValue(res.response.stato);
      this.formgroup.controls.durata.setValue(res.response.durata);
      this.formgroup.controls.luogo.setValue(res.response.luogo);
      this.formgroup.controls.edizione.setValue(res.response.edizione);
      this.formgroup.controls.argomento.setValue(res.response.argomento);
      this.formgroup.controls.dataI.setValue(res.response.dataI);
      this.formgroup.controls.dataF.setValue(res.response.dataF);
      this.formgroup.controls.tipoDocente.setValue(res.response.tipoDocente);
      this.formgroup.controls.descrizione.setValue(res.response.descrizione);
      this.formgroup.controls.interno.setValue(res.response.interno);
      this.formgroup.controls.note.setValue(res.response.note);
      this.formgroup.controls.alunni.setValue(res.response.alunni);
    });
    this.valutazioniService.GetValutazioniCorso(this.id).subscribe((res2) => {
      this.mediacres = res2.response.crescitaProfessionale;
      this.medialav = res2.response.lavoro_attuale;
      this.mediaarg = res2.response.argomenti;
      this.mediaapp = res2.response.approfondimento;
      this.mediadur = res2.response.durata;
      this.mediadiv = res2.response.divisioneTeoriaPratica;
      this.mediaese = res2.response.esercitazioni;
      this.mediamat = res2.response.materiale;
      this.mediacor = res2.response.media;
    });
  }

  selezionaArgomento(tipo) {
    this.tipiSelection = tipo;
    console.log(this.tipiSelection);
  }

  selezionaTipoDocente() {
    this.tipoDocente = !this.tipoDocente;
  }

  filtraDocente(event: any) {
    console.log("tipo docente: ", this.tipoDocente);
    if (event.length > 3 && this.tipoDocente != undefined) {
      if (this.tipoDocente == "1") {
        this.docentiService
          .getDocentiByRagioneSociale(event)
          .subscribe((res) => {
            console.log("lista docenti: ", res.response);
            this.listaDocenti = res.response;
          });
      } else {
        this.dipendentiService
          .getFilter(event)
          .subscribe((res) => (this.listaDocenti = res.response));
      }
    }
  }

  filtraAlunni(event: any) {
    if (event.length > 3) {
      this.dipendentiService.getFilter(event).subscribe((res) => {
        this.listaDipendenti = res.response;
        this.selettoreAlunni.open();
      });
    }
  }

  selezionaDocente(label: any, id: any) {
    this.docente = label;
    this.docenteId = id;
  }

  addAlunno(elemento) {
    let ref = this.dialog.open(AddDialogComponent, {
      width: "500px",
      data: this.id,
    });

    ref.afterClosed().subscribe((res) => {
      let alunni: number[] = [];
      res.forEach((element) => {
        alunni.push(element);
      });
      let obj = {
        alunni: alunni,
      };
      this.alunniService.insertMultiple(this.id, obj).subscribe((res) => {
        this.item = [];
        this.alunniService.getByCorsoId(this.id).subscribe((res) => {
          res.response.forEach((element) => {
            this.item.push(element.dipendente);
          });
        });
      });
      console.log("array int: ", alunni);
    });
  }

  onDeleteHandler(event) {
    console.log("id: ", event);
    this.alunniService.deleteById(event, this.id).subscribe((res) => {
      this.item = [];
      this.alunniService.getByCorsoId(this.id).subscribe((res) => {
        res.response.forEach((element) => {
          this.item.push(element.dipendente);
        });
      });
    });
  }

  conferma() {
    let previsto;
    let erogato;
    if (this.stato) {
      previsto = 1;
      erogato = 0;
    } else {
      previsto = 0;
      erogato = 1;
    }

    if (this.misura) this.misura = 1;
    else this.misura = 2;

    console.log("tipo di docente: ", this.tipoDocente);

    let obj = {
      corso_id: this.id,
      tipo: this.tipiSelection,
      tipologia: this.tipologia,
      descrizione: this.descrizione,
      edizione: this.edizione,
      previsto: previsto,
      erogato: erogato,
      durata: this.durata,
      misura: this.misura,
      note: this.note,
      luogo: this.luogo,
      ente: this.docenteId,
      data_erogazione: this.dataI,
      data_chiusura: this.dataF,
      data_censimento: null,
      interno: this.interno,
    };

    console.log("form prima dell' invio: ", obj);

    this.corso
      .replace(obj, this.tipoDocente)
      .subscribe((res) => this.router.navigate(["corsi/tutto"]));
  }
}
