import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCorsoComponent } from './edit-corso.component';

describe('EditCorsoComponent', () => {
  let component: EditCorsoComponent;
  let fixture: ComponentFixture<EditCorsoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCorsoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCorsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
