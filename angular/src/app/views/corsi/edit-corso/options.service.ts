import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {

  constructor() { }

  public tableOptions = {
    colsOptions: [
      {
        label: "Cognome",
        name: "cognome",
      },
      {
        label: "Nome",
        name: "nome",
      },
      {
        label: "Matricola",
        name: "matricola",
      }
    ],
  };

  public tipoCorso = [
    {
      id: "2",
      name: "aggiornamento",
      label: "Corso d' Aggiornamento",
    },
    {
      id: "1",
      name: "ingresso",
      label: "Corso d' Ingresso",
    },
    {
      id: "3",
      name: "seminario",
      label: "Seminario",
    },
  ];
}
