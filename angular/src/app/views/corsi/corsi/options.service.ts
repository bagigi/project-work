import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class OptionsService {
  constructor() { }

  public tableOptions = {
    colsOptions: [
      {
        label: "Argomento",
        name: "tipo",
      },
      {
        label: "Docente",
        name: "ente",
      },
      {
        label: "Tipologia",
        name: "tipologia",
      },
      {
        label: "Data di Inizio",
        name: "data_erogazione",
      },
      {
        label: "Data di Fine",
        name: "data_chiusura",
      },
    ],
  };

  public deleteOptions: [
    {
      value: "yes",
      label: "Yes",
    },
    {
      value: "no",
      label: "No",
    },
  ];
}
