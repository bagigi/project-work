import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CorsiService } from "../../../../core/service/corsi.service";
import { OptionsService } from "./options.service";
import { AlertDialogComponent } from "../../../shared/alert-dialog/alert-dialog.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-corsi",
  templateUrl: "./corsi.component.html",
  styleUrls: ["./corsi.component.css"],
})
export class CorsiComponent implements OnInit {
  public tableOptions: any;
  public listaagg: any;
  public listasem: any;
  public listaing: any;
  public pages: any;

  constructor(
    public options: OptionsService,
    public router: Router,
    public corsiService: CorsiService,
    public api: CorsiService,
    public dialog: MatDialog
  ) {
    this.tableOptions = options.tableOptions;

    this.corsiService.getPages().subscribe((res) => {
      this.pages = res.response;
    });

    /* this.corsiService.getByPage(1).subscribe((res) => {
      this.listaagg = res.response;
      console.log("lista: ", this.listaagg);
    }); */
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.corsiService.getByTipologia(1).subscribe((res) => {
      this.listaing = res.response;
    });

    this.corsiService.getByTipologia(2).subscribe((res) => {
      this.listaagg = res.response;
    });

    this.corsiService.getByTipologia(3).subscribe((res) => {
      this.listasem = res.response;
    });
  }

  onVoto(id: any) {
    console.log(id);
    this.router.navigate(["valutazioni/home", id]);
  }

  onEdit(id: any) {
    this.router.navigate(["corsi/info", id]);
  }

  onDeleteHandler(id: any) {
    let testo: string;

    this.corsiService.getRelazioni(id).subscribe((res) => {
      console.log(res);
      if (res.response) {
        testo =
          "Questo corso ha degli utenti o degli alunni o delle valutazioni associate, proseguire?";
      } else {
        testo = "Sei sicuro di voler eliminare il corso?";
        console.log("dio canaglia coda di paglia");
      }
      let ref = this.dialog.open(AlertDialogComponent, {
        width: "500px",
        data: {
          options: [
            {
              value: "yes",
              label: "Yes",
            },
            {
              value: "no",
              label: "No",
            },
          ],
          title: "Elimina Corso",
          testo: testo,
        },
      });
      ref.afterClosed().subscribe((ciccio) => {
        if (ciccio == "yes") {
          this.api.deleteById(id).subscribe((r) => {
            this.getData();
          });
        }
      });
    });
  }
}
