import { Injectable } from "@angular/core";
import { CorsiService } from "../../../../core/service/corsi.service";

@Injectable({
  providedIn: "root",
})
export class OptionsService {
  public tipoCorso: any;
  constructor() {
    this.tipoCorso = [
      {
        id: "2",
        name: "aggiornamento",
        label: "Corso d' Aggiornamento",
      },
      {
        id: "1",
        name: "ingresso",
        label: "Corso d' Ingresso",
      },
      {
        id: "3",
        name: "seminario",
        label: "Seminario",
      },
    ];
  }
}
