import { Component, OnInit, ViewChild } from "@angular/core"
import { OptionsService } from "./options.service"
import { DocentiService } from "../../../../core/service/docenti.service"
import { DipendentiService } from "../../../../core/service/dipendenti.service"
import { FormBuilder, FormGroup, FormControl } from "@angular/forms"
import { TipiService } from "../../../../core/service/tipi.service"
import { CorsiService } from '../../../../core/service/corsi.service'
import { AlunniService } from '../../../../core/service/alunni.service'
import { Router, ActivatedRoute } from '@angular/router'


@Component({
  selector: "app-new-corso",
  templateUrl: "./new-corso.component.html",
  styleUrls: ["./new-corso.component.css"],
})
export class NewCorsoComponent implements OnInit {
  @ViewChild('selettoreAlunni') selettoreAlunni;

  public tipologia: String;
  public statoCorso: boolean = true; // false = erogato, true = previsto
  public statoDurata: boolean = true; //true = giorni, false = ore
  public listaDocenti: any[];
  public docentiCopia: any;
  public listaDipendenti: any[];
  public dipendentiCopia: any;
  public listaTipi: any;
  public tipiSelection: any;
  public dataI: any;
  public dataF: any;
  public descrizione: any;
  public note: any;
  public tipoDocente: any;
  public selezioneDocente: any;
  public interno: any;
  public alunniSelezionati: number[];

  public formgroup: FormGroup;



  constructor(
    public options: OptionsService,
    public docentiService: DocentiService,
    public dipendentiService: DipendentiService,
    public corsiService: CorsiService,
    public alunniService: AlunniService,
    public fb: FormBuilder,
    public tipiService: TipiService,
    public routeActive: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit(): void {

    this.dipendentiService.getAll().subscribe(res => {
      this.listaDipendenti = res.response;
    });

    this.tipiService
      .getAll()
      .subscribe((res) => { (this.listaTipi = res.response) })

    this.createForm();
  }

  createForm() {
    this.formgroup = new FormGroup({
      tipo: new FormControl(""),
      tipologia: new FormControl(""),
      descrizione: new FormControl(""),
      edizione: new FormControl(""),
      previsto: new FormControl(""),
      erogato: new FormControl(""),
      durata: new FormControl(""),
      misura: new FormControl(""),
      note: new FormControl(""),
      luogo: new FormControl(""),
      ente: new FormControl(""),
      data_erogazione: new FormControl(""),
      data_chiusura: new FormControl(""),
      interno: new FormControl(""),
    });
  }

  stampa() {
    console.log("selezione: ", this.tipologia)
  }

  selezionaArgomento(tipo) {
    this.tipiSelection = tipo;
    console.log(this.tipiSelection);
  }

  selezionaDocente(docente: any) {
    console.log("yo: ", docente);
    this.selezioneDocente = docente;
  }

  filtraDocente(event: any) {
    if (event.length > 3 && this.tipoDocente != undefined) {
      if (this.tipoDocente == "esterno") {
        this.docentiService.getDocentiByRagioneSociale(event).subscribe(res => {
          this.listaDocenti = res.response;
        });
      } else {
        this.dipendentiService.getFilter(event).subscribe(res => this.listaDocenti = res.response);
      }
    }
  }

  filtraAlunni(event: any) {
    if (event.length > 3) {
      this.dipendentiService.getFilter(event).subscribe(res => {
        this.listaDipendenti = res.response;
        this.selettoreAlunni.open();
      });
    }
  }

  cliccatoStato() {
    this.statoCorso = !this.statoCorso
    console.log("statO: ", this.statoCorso)
  }

  cliccatoDurata() {
    this.statoDurata = !this.statoDurata
  }

  invia() {

    let erogato: number
    let previsto: number
    let misura: number

    if (this.statoCorso) {
      erogato = 0
      previsto = 1
    } else {
      erogato = 1
      previsto = 0
    }

    if (this.statoDurata) misura = 2
    else misura = 1

    if (!this.interno) this.interno = 0
    else this.interno = 1

    if (this.tipoDocente == "esterno") this.tipoDocente = 1
    else this.tipoDocente = 0;

    console.log("interno: ", this.interno);

    this.dataF = this.dataF.toISOString();
    this.dataI = this.dataI.toISOString();

    this.dataF = this.dataF.substring(0, this.dataF.indexOf("T"));
    this.dataI = this.dataI.substring(0, this.dataI.indexOf("T"));

    this.formgroup = this.fb.group({
      tipo: [this.tipiSelection],
      tipologia: [this.tipologia],
      descrizione: [this.descrizione],
      edizione: [this.formgroup.value.edizione],
      previsto: [previsto],
      erogato: [erogato],
      durata: [this.formgroup.value.durata],
      misura: [misura],
      note: [this.note],
      luogo: [this.formgroup.value.luogo],
      data_erogazione: [this.dataI],
      data_chiusura: [this.dataF],
      interno: [this.interno],
    })
    //console.log(this.formgroup.value.data_chiusura.toISOString());
    console.log(this.formgroup.value);
    console.log("alunni selezionati: ", this.alunniSelezionati);
    this.corsiService.newCorso(this.tipoDocente, this.selezioneDocente, this.formgroup.value).subscribe(res => {
      let bb = {
        alunni: this.alunniSelezionati
      }
      this.alunniService.insertMultiple(res.response, bb).subscribe();
    });



    this.router.navigateByUrl("corsi/tutto");

  }
}
