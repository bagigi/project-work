import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCorsoComponent } from './new-corso.component';

describe('NewCorsoComponent', () => {
  let component: NewCorsoComponent;
  let fixture: ComponentFixture<NewCorsoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCorsoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCorsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
