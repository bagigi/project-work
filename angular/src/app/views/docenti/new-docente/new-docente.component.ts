import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import { DocentiService } from '../../../../core/service/docenti.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: "app-new-corso",
  templateUrl: "./new-docente.component.html",
  styleUrls: ["./new-docente.component.css"],
})


export class NewDocenteComponent implements OnInit {
  public formgroup: FormGroup;


  constructor(
    public fb: FormBuilder,
    public docente: DocentiService,
    public routeActive: ActivatedRoute,
    public router: Router
  ) {

  }

  ngOnInit(): void {

    this.formgroup = this.fb.group({
      nome: [""],
      cognome: [""],
      titolo: [""],
      ragioneSociale: [""],
      indirizzo: [""],
      localita: [""],
      provincia: [""],
      nazione: [""],
      telefono: [""],
      fax: [""],
      partita_iva: [""],
      referente: [""],

    });
  }

  conferma() {



    console.log("form prima dell' invio: ", this.formgroup.value);

    this.docente.add(this.formgroup.value).subscribe((k) => {
      this.router.navigateByUrl("docenti")
    })

  }


}