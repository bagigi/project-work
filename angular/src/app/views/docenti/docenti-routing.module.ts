import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocentiComponent } from "./docenti/docenti.component";
import { NewDocenteComponent } from './new-docente/new-docente.component';
import { EditDocenteComponent } from './edit-docente/edit-docente.component';
import { DocentiInterniComponent } from './docenti-interni/docenti-interni.component';

const routes: Routes = [
  {
    path: "",
    component: DocentiComponent,
    data: {
      title: "Docenti"
    }
  },
  {
    path: "interni",
    component: DocentiInterniComponent,
    data: {
      title: "Docenti Interni"
    }
  },
  {
    path: "nuovo",
    component: NewDocenteComponent,
    data: {
      title: "Nuovo Docente",
    },
  },
  {
    path: "edit/:id",
    component: EditDocenteComponent,
    data: {
      title: "Modifica Docente",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocentiRoutingModule { }
