import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DocentiRoutingModule } from "./docenti-routing.module";
import { DocentiComponent } from "./docenti/docenti.component";
import { SharedModule } from "../../shared/shared/shared.module";
import { DialogComponent } from "../../shared/dialog/dialog.component";
import {
  FormsModule,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
} from "@angular/forms";
import { MatFormField, MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from "@angular/material/dialog";
import { PageSelectorComponent } from "../../shared/page-selector/page-selector.component";
import { AlertDialogComponent } from "../../shared/alert-dialog/alert-dialog.component";
import { NewDocenteComponent } from "./new-docente/new-docente.component";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { InfoDialogComponent } from "../../shared/info-dialog/info-dialog.component";
import { EditDocenteComponent } from "./edit-docente/edit-docente.component";
import { DocentiInterniComponent } from "./docenti-interni/docenti-interni.component";

@NgModule({
  declarations: [
    DocentiComponent,
    DialogComponent,
    NewDocenteComponent,
    InfoDialogComponent,
    EditDocenteComponent,
    DocentiInterniComponent,
  ],
  imports: [
    CommonModule,
    DocentiRoutingModule,
    SharedModule,
    FormsModule,
    MatFormFieldModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  entryComponents: [DialogComponent, AlertDialogComponent, InfoDialogComponent],
})
export class DocentiModule {}
