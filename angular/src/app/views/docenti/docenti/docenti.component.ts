import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../../../../core/service/api.service";
import { DocentiService } from "../../../../core/service/docenti.service";
import { TabellaOptions } from "../../../api/api-options";

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { DialogComponent } from "../../../shared/dialog/dialog.component";
import { AlertDialogComponent } from "../../../shared/alert-dialog/alert-dialog.component";
import { InfoDialogComponent } from "../../../shared/info-dialog/info-dialog.component";

@Component({
  selector: "app-docenti",
  templateUrl: "./docenti.component.html",
  styleUrls: ["./docenti.component.css"],
})
export class DocentiComponent implements OnInit {
  public path = "api/docenti/";

  public lista: any[];
  public copy: any[];
  public pages: any[];
  public filtro: any[] = null;

  options: TabellaOptions = {
    colsOptions: [
      {
        label: "Nome",
        name: "nome",
      },
      {
        label: "Cognome",
        name: "cognome",
      },
      {
        label: "Titolo",
        name: "titolo",
      },
      {
        label: "Ragione sociale",
        name: "ragione_sociale",
      },
    ],
  };

  public keys = [
    {
      label: "Nome",
      name: "nome",
    },
    {
      label: "Cognome",
      name: "cognome",
    },
    {
      label: "Titolo",
      name: "titolo",
    },
    {
      label: "Ragione sociale",
      name: "ragione_sociale",
    },
  ];

  public DialogOptions: any[] = [
    {
      name: "nome",
      label: "Nome: ",
    },
    {
      name: "cognome",
      label: "Cognome: ",
    },
    {
      name: "titolo",
      label: "Titolo: ",
    },
    {
      name: "ragione_sociale",
      label: "Ragione sociale: ",
    },
    {
      name: "indirizzo",
      label: "Indirizzo: ",
    },
    {
      name: "località",
      label: "Località: ",
    },
    {
      name: "provincia",
      label: "provincia: ",
    },
    {
      name: "nazione",
      label: "Nazione: ",
    },
    {
      name: "telefono",
      label: "Telefono: ",
    },
    {
      name: "fax",
      label: "Fax: ",
    },
    {
      name: "partita_iva",
      label: "Partita iva: ",
    },
    {
      name: "referente",
      label: "Referente: ",
    },
  ];
  constructor(
    public routeActive: ActivatedRoute,
    public docentiService: DocentiService,
    public router: Router,
    public api: DocentiService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.docentiService.getByPage(1).subscribe((res) => {
      this.lista = res.response;
      this.copy = res.response;
    });

    this.docentiService.getPages().subscribe((res) => {
      this.pages = res.response;
      console.log("Numero di pagine: ", res.response);
    });
  }

  select(input: any[]) {
    const sogg = input[0];
    this.router.navigate(["docenti", sogg.docente_id]);
  }

  filter(res: any) {
    if (res == -1) {
      this.docentiService.getByPage(1).subscribe((res) => {
        this.lista = res.response;
        this.copy = res.response;
        this.docentiService
          .getPages()
          .subscribe((res) => (this.pages = res.response));
      });

      this.filtro = null;
    } else {
      this.filtro = res;
      console.log(res, this.filtro);
      let percorsoPages: String =
        "/info/pages/filter?filter=" + res[0] + "&value=" + res[1];
      let percorsoFilter: String =
        "/page/filter?filter=" + res[0] + "&value=" + res[1] + "&page=1";

      this.docentiService.getFilterPages(percorsoPages).subscribe((res) => {
        this.pages = res.response;
        this.docentiService.getFilterList(percorsoFilter).subscribe((res) => {
          this.lista = res.response;
        });
      });
    }
  }

  aggiorna(event) {
    if (this.filtro != null) {
      let percorsoFilter: String =
        "/page/filter?filter=" + this.filtro[0] + "&value=" + this.filtro[1];
      this.docentiService
        .getFilterList(percorsoFilter + "&page=" + event)
        .subscribe((res) => {
          this.lista = res.response;
        });
    } else {
      this.docentiService.getByPage(event).subscribe((res) => {
        this.lista = res.response;
        this.copy = res.response;
      });
    }
  }

  addUser() {
    let ref = this.dialog.open(DialogComponent, {
      width: "500px",
      data: { options: this.DialogOptions, title: "Nuovo Docente" },
    });

    ref.afterClosed().subscribe((result) => {
      console.log("result: ", result);

      let obj = {
        nome: result.nome,
        cognome: result.cognome,
        titolo: result.titolo,
        ragioneSociale: result.ragione_sociale,
        indirizzo: result.indirizzo,
        provincia: result.provincia,
        nazione: result.nazione,
        telefono: result.telefono,
        fax: result.fax,
        partita_iva: result.partita_iva,
        referente: result.referente,
      };

      console.log("oggetto inviato: ", obj);

      this.api.add(obj).subscribe((k) => {
        this.api.getPages().subscribe((j) => {
          this.pages = j.response;
          let len = Object.keys(j.response).length;
          let p = j.response[len - 1];
          this.api.getByPage(p).subscribe((l) => {
            this.lista = l.response;
          });
        });
      });
    });
  }

  onDeleteHandler(id: any) {
    let testo = "Vuoi davvero cancellare il docente?";
    this.docentiService.getRelazioni(id, 1).subscribe((res) => {
      if (res.response)
        testo =
          "Ci sono delle valutazioni o dei corsi associati a questo utente, proseguire ugualmente?";
      let ref = this.dialog.open(AlertDialogComponent, {
        width: "500px",
        data: {
          options: [
            {
              value: "yes",
              label: "Yes",
            },
            {
              value: "no",
              label: "No",
            },
          ],
          title: "Elimina Docente",
          testo: "Vuoi davvero cancellare l' utente?",
        },
      });

      ref.afterClosed().subscribe((ciccio) => {
        if (ciccio == "yes") {
          this.api.deleteById(id).subscribe((r) => {
            this.api.getPages().subscribe((j) => {
              this.pages = j.response;
              let len = Object.keys(j.response).length;
              let p = j.response[len - 1];
              this.api.getByPage(p).subscribe((l) => {
                this.lista = l.response;
              });
            });
          });
        }
      });
    });
  }

  onEditHandler(id: any) {
    /* let kk: any = this.lista[id]
    console.log("lista: ", this.lista)
    console.log("passo: ", kk)
    console.log("id: ", kk.id) */
    this.router.navigate(["docenti/edit", id]);
  }
}
