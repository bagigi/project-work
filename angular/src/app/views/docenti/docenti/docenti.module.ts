import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DocentiRoutingModule } from "./docenti-routing.module";
import { DocentiComponent } from "./docenti.component";
import { TabellaComponent } from "../../../shared/tabella/tabella.component";
import { FilterComponent } from "../../../shared/filtra/filtra.component";
import { DialogComponent } from "../../../shared/dialog/dialog.component";
import { FormsModule } from "@angular/forms";
import { PageSelectorComponent } from "../../../shared/page-selector/page-selector.component";

@NgModule({
  declarations: [
    DocentiComponent,
    TabellaComponent,
    FilterComponent,
    DialogComponent,
    FormsModule,
    PageSelectorComponent,
  ],
  imports: [CommonModule, DocentiRoutingModule],
  entryComponents: [DialogComponent],
})
export class DocentiModule {}
