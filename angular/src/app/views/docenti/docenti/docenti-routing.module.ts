import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocentiComponent } from './docenti.component';

const routes: Routes = [{ path: '', component: DocentiComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocentiRoutingModule { }
