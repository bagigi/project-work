import { Component, OnInit } from "@angular/core";
import { DocentiService } from "../../../../core/service/docenti.service";
import { OptionsService } from "./options.service";
import { TabellaOptions } from "../../../api/api-options";
import { Router } from "@angular/router";

@Component({
  selector: "app-docenti-interni",
  templateUrl: "./docenti-interni.component.html",
  styleUrls: ["./docenti-interni.component.css"],
})
export class DocentiInterniComponent implements OnInit {
  public pages: any[];
  public docenti: any[];
  public copy: any[];

  public tableOptions: TabellaOptions;
  public filterOptions: any[];

  public filtro: any;

  constructor(
    public docentiService: DocentiService,
    public options: OptionsService,
    public router: Router
  ) {
    this.tableOptions = this.options.options;
    this.filterOptions = this.options.keys;
  }

  ngOnInit(): void {
    this.docentiService
      .getDocentiInterniPages()
      .subscribe((res) => (this.pages = res.response));
    this.docentiService
      .getDocentiInterni(1)
      .subscribe((res) => (this.docenti = res.response));
  }

  info(id: any) {
    this.router.navigate(["dipendenti/info", id]);
  }

  filter(res: any) {
    console.log("res: ", res);
    if (res == -1) {
      this.docentiService.getDocentiInterni(1).subscribe((res) => {
        this.docenti = res.response;
        this.copy = res.response;
        this.docentiService
          .getDocentiInterniPages()
          .subscribe((res) => (this.pages = res.response));
      });

      this.filtro = null;
    } else {
      this.filtro = res;
      console.log(res, this.filtro);

      this.docentiService
        .getDocentiInterniFilterPage(res[0], res[1])
        .subscribe((res) => {
          this.pages = res.response;
          this.docentiService
            .getDocentiByFilter(res[0], res[1], 1)
            .subscribe((res) => {
              this.docenti = res.response;
            });
        });
    }
  }

  aggiorna(event) {
    if (this.filtro != null) {
      let percorsoFilter: String =
        "/page/filter?filter=" + this.filtro[0] + "&value=" + this.filtro[1];
      this.docentiService
        .getFilterList(percorsoFilter + "&page=" + event)
        .subscribe((res) => {
          this.docenti = res.response;
        });
    } else {
      this.docentiService.getByPage(event).subscribe((res) => {
        this.docenti = res.response;
        this.copy = res.response;
      });
    }
  }
}
