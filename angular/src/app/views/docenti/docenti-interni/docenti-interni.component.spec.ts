import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocentiInterniComponent } from './docenti-interni.component';

describe('DocentiInterniComponent', () => {
  let component: DocentiInterniComponent;
  let fixture: ComponentFixture<DocentiInterniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocentiInterniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocentiInterniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
