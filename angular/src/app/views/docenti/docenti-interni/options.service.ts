import { Injectable } from '@angular/core';
import { TabellaOptions } from '../../../api/api-options';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {

  constructor() { }

  public keys = [
    {
      label: "Nome",
      name: "nome",
    },
    {
      label: "Cognome",
      name: "cognome",
    },
    {
      label: "Titolo",
      name: "titolo",
    },
    {
      label: "Ragione sociale",
      name: "ragione_sociale",
    },
  ];

  options: TabellaOptions = {
    colsOptions: [
      {
        label: "Nome",
        name: "nome",
      },
      {
        label: "Cognome",
        name: "cognome",
      },
      {
        label: "Qualifica",
        name: "qualifica",
      },
      {
        label: "Livello",
        name: "livello",
      },
    ],
  }
}
