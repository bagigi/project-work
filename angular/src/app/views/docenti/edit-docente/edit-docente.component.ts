import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from '@angular/forms';
import { DocentiService } from '../../../../core/service/docenti.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TabellaOptions } from '../../../api/api-options';
import { ValutazioniService } from '../../../../core/service/valutazioni.service';

@Component({
  selector: "app-edit-docente",
  templateUrl: "./edit-docente.component.html",
  styleUrls: ["./edit-docente.component.css"],
})


export class EditDocenteComponent implements OnInit {
  public formgroup: FormGroup;

  public soggetto;
  public id;
  public listavoti: any[];
  public lista: any[]
  public copy: any[]

  options: TabellaOptions = {
    colsOptions: [
      {
        label: "Nome Corso",
        name: "corso",
      },

      {
        label: "Media Voto",
        name: "media",
      },
    ],
  }


  public nome;
  public cognome;
  public ragioneSociale;
  public titolo;
  public telefono;
  public fax;
  public partitaIva;
  public referente;

  constructor(
    public fb: FormBuilder,
    public docente: DocentiService,
    public routeActive: ActivatedRoute,
    public router: Router,
    public voti: ValutazioniService,
  ) {

  }

  ngOnInit(): void {
    this.id = this.routeActive.snapshot.params.id;
    this.voti.GetValutazioniDocente(this.id, "esterno", 1).subscribe((res2) => {
      this.lista = res2.response
      this.copy = res2.response
    })

    this.formgroup = this.fb.group({
      nome: [""],
      cognome: [""],
      titolo: [""],
      ragione_sociale: [""],
      telefono: [""],
      fax: [""],
      partita_iva: [""],
      referente: [""]
    });

    this.id = this.routeActive.snapshot.params.id;
    this.docente.getById(this.id).subscribe(res => {

      console.log(res.response)

      this.formgroup.controls.nome.setValue(res.response.nome)
      this.formgroup.controls.cognome.setValue(res.response.cognome)
      this.formgroup.controls.titolo.setValue(res.response.titolo)
      this.formgroup.controls.ragione_sociale.setValue(res.response.ragione_sociale)
      this.formgroup.controls.telefono.setValue(res.response.telefono)
      this.formgroup.controls.fax.setValue(res.response.fax)
      this.formgroup.controls.partita_iva.setValue(res.response.partita_iva)
      this.formgroup.controls.referente.setValue(res.response.referente)

    })

  }


  conferma() {
    this.formgroup = this.fb.group({
      nome: [this.formgroup.value.nome],
      cognome: [this.formgroup.value.cognome],
      titolo: [this.formgroup.value.titolo],
      ragione_sociale: [this.formgroup.value.ragione_sociale],
      telefono: [this.formgroup.value.telefono],
      fax: [this.formgroup.value.fax],
      partita_iva: [this.formgroup.value.partita_iva],
      referente: [this.formgroup.value.referente]
    });

    console.log("form prima dell' invio: ", this.formgroup.value);

    this.docente.replace(this.formgroup.value, this.id).subscribe(res => this.router.navigate(["docenti"]));
  }


}
