import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValutazioniDipendentiComponent } from './valutazioni-dipendenti.component';

describe('ValutazioniDipendentiComponent', () => {
  let component: ValutazioniDipendentiComponent;
  let fixture: ComponentFixture<ValutazioniDipendentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValutazioniDipendentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValutazioniDipendentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
