import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValutazioniDocentiComponent } from './valutazioni-docenti.component';

describe('ValutazioniDocentiComponent', () => {
  let component: ValutazioniDocentiComponent;
  let fixture: ComponentFixture<ValutazioniDocentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValutazioniDocentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValutazioniDocentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
