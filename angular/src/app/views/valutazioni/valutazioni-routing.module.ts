import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ValutazioniCorsiComponent } from './valutazioni-corsi/valutazioni-corsi.component';
import { ValutazioniDipendentiComponent } from './valutazioni-dipendenti/valutazioni-dipendenti.component';
import { ValutazioniDocentiComponent } from './valutazioni-docenti/valutazioni-docenti.component';
import { ValutazioniHomeComponent } from './valutazioni-home/valutazioni-home.component';

const routes: Routes = [
  {
    path: "corsi",
    component: ValutazioniCorsiComponent,
    data: {
      title: "Valutazioni Corsi",
    },
  },
  {
    path: "docenti",
    component: ValutazioniDocentiComponent,
    data: {
      title: "Valutazioni Docenti",
    },
  },
  {
    path: "dipendenti",
    component: ValutazioniDipendentiComponent,
    data: {
      title: "Valutazioni Dipendenti",
    },
  },
  {
    path: "home/:id",
    component: ValutazioniHomeComponent,
    data: {
      title: "Home Valutazione",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValutazioniRoutingModule { }
