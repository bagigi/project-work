import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared/shared.module';
import { ValutazioniHomeComponent } from './valutazioni-home/valutazioni-home.component';
import { ValutazioniRoutingModule } from './valutazioni-routing.module';
import { ValutazioniCorsiComponent } from './valutazioni-corsi/valutazioni-corsi.component';
import { ValutazioniDipendentiComponent } from './valutazioni-dipendenti/valutazioni-dipendenti.component';
import { ValutazioniDocentiComponent } from './valutazioni-docenti/valutazioni-docenti.component';
import { MatLabel, MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [ValutazioniHomeComponent, ValutazioniCorsiComponent, ValutazioniDipendentiComponent, ValutazioniDocentiComponent],
  imports: [
    CommonModule, 
    ValutazioniRoutingModule, 
    SharedModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    MatFormFieldModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatButtonModule,
  ]
})
export class ValutazioniModule { }