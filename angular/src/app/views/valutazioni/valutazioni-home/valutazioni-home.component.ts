import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { DocentiService } from "../../../../core/service/docenti.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CorsiService } from "../../../../core/service/corsi.service";
import { OptionsService } from "./options.service";
import { ValutazioniService } from "../../../../core/service/valutazioni.service";
import { AlunniService } from "../../../../core/service/alunni.service";

@Component({
  selector: "app-valutazioni-home",
  templateUrl: "./valutazioni-home.component.html",
  styleUrls: ["./valutazioni-home.component.css"],
})
export class ValutazioniHomeComponent implements OnInit {
  public corsoId: number;
  public docenteId: any;
  public tipoDocente: any;
  public corso: any;

  public optionDocente: any;

  public formgroup: FormGroup;
  public formDocente: FormGroup;
  public formCorso: FormGroup;
  public formUtente: FormGroup;

  public mediacomp: any;
  public mediachi: any;
  public mediadisp: any;
  public mediadoc: any;

  public optionCorso: any;

  public mediacres: any;
  public medialav: any;
  public mediaarg: any;
  public mediaapp: any;
  public mediadur: any;
  public mediadiv: any;
  public mediaese: any;
  public mediamat: any;
  public mediacor: any;
  public mediadip: any;
  public utentesel: any;

  public voto1: number;
  public voto2: number;
  public voto3: number;
  public voto4: number;
  public voto5: number;

  public idValDocente: any;
  public idValCorso: any;
  public idValAlunno: any;

  public idAlunno: any;

  public listautenti: any;

  constructor(
    public fb: FormBuilder,
    public docente: DocentiService,
    public routeActive: ActivatedRoute,
    public router: Router,
    public corsiService: CorsiService,
    public optionsService: OptionsService,
    public valutazioniService: ValutazioniService,
    public alunniService: AlunniService
  ) {
    this.corsoId = this.routeActive.snapshot.params.id;
    this.optionDocente = this.optionsService.docente;
    this.optionCorso = this.optionsService.corso;

    this.alunniService.getByCorsoId(this.corsoId).subscribe((res) => {
      this.listautenti = res.response;
      console.log("lista:", this.listautenti);
    });
  }

  ngOnInit(): void {
    this.scaricaDati();

    this.formDocente = this.fb.group({
      id: [""],
      docente: [""],
      corso: [""],
      completezza: [""],
      chiarezza: [""],
      disponibilita: [""],
      media: [""],
    });

    //this.corsiService.getById(this.corsoId).subscribe(res => this.corsoId = res.response);

    this.formCorso = this.fb.group({
      id: [""],
      crescita_professionale: [""],
      lavoro_attuale: [""],
      argomenti: [""],
      approfondimento: [""],
      durata: [""],
      divisione_teoria_pratica: [""],
      esercitazioni: [""],
      materiale_didattico: [""],
      responsabile: [""],
      media: [""],
      corso: [""],
    });

    this.formUtente = this.fb.group({
      id: [""],
      impegno: [""],
      logica: [""],
      apprendimento: [""],
      velocita: [""],
      ordine_precisione: [""],
      responsabile: [""],
      media: [""],
      dipendente: [""],
      corso: [""],
    });
  }

  reset() {
    this.mediacomp = 0;
    this.mediachi = 0;
    this.mediadisp = 0;
    this.mediadoc = 0;
    this.idValDocente = null;
    this.mediacres = 0;
    this.medialav = 0;
    this.mediaarg = 0;
    this.mediaapp = 0;
    this.mediadur = 0;
    this.mediadiv = 0;
    this.mediaese = 0;
    this.mediamat = 0;
    this.mediacor = 0;
    this.idValCorso = null;
    this.mediadip = 0;
    this.idValAlunno = null;
  }

  scaricaDati() {
    this.reset();

    this.corsiService.getDocenteId(this.corsoId).subscribe((res) => {
      this.docenteId = res.response;
      this.corsiService.getTipoDocente(this.corsoId).subscribe((res) => {
        this.tipoDocente = res.response;
        this.valutazioniService
          .getValutazioneDocenteByCorso(this.docenteId, this.corsoId)
          .subscribe((res) => {
            let response = res.response;
            console.log("lel: ", response);
            this.mediacomp = response.completezza;
            this.mediachi = response.chiarezza;
            this.mediadisp = response.disponibilita;
            this.mediadoc = response.media;
            this.idValDocente = response.id;
          });
      });
    });

    this.valutazioniService
      .GetValutazioniCorso(this.corsoId)
      .subscribe((res) => {
        let r = res.response;
        if (r != null) {
          this.mediacres = r.crescitaProfessionale;
          this.medialav = r.lavoro_attuale;
          this.mediaarg = r.argomenti;
          this.mediaapp = r.approfondimento;
          this.mediadur = r.durata;
          this.mediadiv = r.divisioneTeoriaPratica;
          this.mediaese = r.esercitazioni;
          this.mediamat = r.materiale;
          this.mediacor = r.media;
          this.idValCorso = r.id;
        }
      });

    if (this.idAlunno != null) {
      this.valutazioniService
        .getValutazioneDipendenteByCorso(this.idAlunno, this.corsoId)
        .subscribe((res) => {
          this.mediadip = res.response.media;
          this.idValAlunno = res.response.id;
        });
    }
  }

  getUtenteVal(id: any) {
    console.log("test id: ", id);
    this.idAlunno = id;
    this.valutazioniService
      .getValutazioneDipendenteByCorso(id, this.corsoId)
      .subscribe((res) => {
        this.mediadip = res.response.media;
        this.idValAlunno = res.response.id;
      });
  }

  conferma3(v1: number, v2: number, v3: number, v4: number, v5: number) {
    console.log(
      "valore1:",
      v1,
      " valore2: ",
      v2,
      " valore3: ",
      v3,
      " valore4: ",
      v4,
      " valore5: ",
      v5
    );
    console.log((v1 + v2 + v3 + v4 + v5) / 5);
    this.mediadip = ((v1 + v2 + v3 + v4 + v5) / 5).toFixed();

    this.formUtente = this.fb.group({
      id: [this.idValAlunno],
      corso: [this.corsoId],
      dipendente: [this.utentesel.id],
      impegno: [v1],
      logica: [v2],
      apprendimento: [v3],
      velocita: [v4],
      ordine_precisione: [v5],
      media: [((v1 + v2 + v3 + v4 + v5) / 5).toFixed()],
    });

    console.log("vediamo: ", this.utentesel.id);
    this.valutazioniService
      .valutaUtente(this.formUtente.value)
      .subscribe((res) => this.scaricaDati());
  }

  testDoc() {
    let cont = 0;
    let sum = 0;
    for (let i = 0; i != this.optionDocente.completezza.length; i++) {
      if (Number(this.optionDocente.completezza[i])) {
        cont += this.optionDocente.completezza[i];
        sum += this.optionDocente.completezza[i] * i;
      } else cont += 0;
    }
    this.mediacomp = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media comp: " + this.mediacomp, " cont: ", cont);

    for (let i = 0; i != this.optionDocente.chiarezza.length; i++) {
      if (Number(this.optionDocente.chiarezza[i])) {
        cont += this.optionDocente.chiarezza[i];
        sum += this.optionDocente.chiarezza[i] * i;
      } else cont += 0;
    }
    this.mediachi = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionDocente.disponibilita.length; i++) {
      if (Number(this.optionDocente.disponibilita[i])) {
        cont += this.optionDocente.disponibilita[i];
        sum += this.optionDocente.disponibilita[i] * i;
      } else cont += 0;
    }
    this.mediadisp = Math.ceil(sum / cont);

    this.mediadoc = (
      (this.mediacomp + this.mediachi + this.mediadisp) /
      3
    ).toFixed(1);
    /* console.log("docenteid:", this.docenteId.id);
    console.log("corsoid:", this.corsoId); */

    this.formDocente = this.fb.group({
      id: [this.idValDocente],
      docente: [this.docenteId],
      corso: [this.corsoId],
      completezza: [this.mediacomp.toFixed()],
      chiarezza: [this.mediachi.toFixed()],
      disponibilita: [this.mediadisp.toFixed()],
      media: [this.mediadoc],
    });

    this.valutazioniService
      .valutaDocente(this.formDocente.value)
      .subscribe((res) => this.scaricaDati());
  }

  testCor() {
    let cont = 0;
    let sum = 0;
    for (let i = 0; i != this.optionCorso.crescita_professionale.length; i++) {
      if (Number(this.optionCorso.crescita_professionale[i])) {
        cont += this.optionCorso.crescita_professionale[i];
        sum += this.optionCorso.crescita_professionale[i] * i;
      } else cont += 0;
    }
    this.mediacres = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionCorso.lavoro_attuale.length; i++) {
      if (Number(this.optionCorso.lavoro_attuale[i])) {
        cont += this.optionCorso.lavoro_attuale[i];
        sum += this.optionCorso.lavoro_attuale[i] * i;
      } else cont += 0;
    }
    this.medialav = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionCorso.argomenti.length; i++) {
      if (Number(this.optionCorso.argomenti[i])) {
        cont += this.optionCorso.argomenti[i];
        sum += this.optionCorso.argomenti[i] * i;
      } else cont += 0;
    }
    this.mediaarg = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    // console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionCorso.approfondimento.length; i++) {
      if (Number(this.optionCorso.approfondimento[i])) {
        cont += this.optionCorso.approfondimento[i];
        sum += this.optionCorso.approfondimento[i] * i;
      } else cont += 0;
    }
    this.mediaapp = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionCorso.durata.length; i++) {
      if (Number(this.optionCorso.durata[i])) {
        cont += this.optionCorso.durata[i];
        sum += this.optionCorso.durata[i] * i;
      } else cont += 0;
    }
    this.mediadur = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (
      let i = 0;
      i != this.optionCorso.divisione_teoria_pratica.length;
      i++
    ) {
      if (Number(this.optionCorso.divisione_teoria_pratica[i])) {
        cont += this.optionCorso.divisione_teoria_pratica[i];
        sum += this.optionCorso.divisione_teoria_pratica[i] * i;
      } else cont += 0;
    }
    this.mediadiv = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionCorso.esercitazioni.length; i++) {
      if (Number(this.optionCorso.esercitazioni[i])) {
        cont += this.optionCorso.esercitazioni[i];
        sum += this.optionCorso.esercitazioni[i] * i;
      } else cont += 0;
    }
    this.mediaese = Math.ceil(sum / cont);
    sum = 0;
    cont = 0;
    //console.log("media crescita: " + this.mediacres, " cont: ", cont);

    for (let i = 0; i != this.optionCorso.materiale_didattico.length; i++) {
      if (Number(this.optionCorso.materiale_didattico[i])) {
        cont += this.optionCorso.materiale_didattico[i];
        sum += this.optionCorso.materiale_didattico[i] * i;
      } else cont += 0;
    }
    this.mediamat = Math.ceil(sum / cont);

    this.mediacor = (
      (this.mediacres +
        this.medialav +
        this.mediaarg +
        this.mediaapp +
        this.mediadur +
        this.mediadiv +
        this.mediaese +
        this.mediamat) /
      8
    ).toFixed(1);

    this.formCorso = this.fb.group({
      id: [this.idValCorso],
      corso: [this.corsoId],
      crescitaProfessionale: [this.mediacres.toFixed()],
      lavoro_attuale: [this.medialav.toFixed()],
      argomenti: [this.mediaarg.toFixed()],
      approfondimento: [this.mediaapp.toFixed()],
      durata: [this.mediadur.toFixed()],
      divisioneTeoriaPratica: [this.mediadiv.toFixed()],
      esercitazioni: [this.mediaese.toFixed()],
      materiale: [this.mediamat.toFixed()],
      media: [this.mediacor],
    });

    console.log("form corso: ", this.formCorso.value);

    this.valutazioniService
      .valutaCorso(this.formCorso.value)
      .subscribe((res) => this.scaricaDati());
  }

  elimina(tipo: any) {
    console.log(tipo);
    switch (tipo) {
      case "alunno":
        this.valutazioniService
          .deleteValutazioneById(this.idValAlunno, "alunno")
          .subscribe((res) => this.scaricaDati());
        break;
      case "docente":
        this.valutazioniService
          .deleteValutazioneById(this.idValDocente, "docente")
          .subscribe((res) => this.scaricaDati());
        break;
      case "corso":
        console.log("dio boia");
        this.valutazioniService
          .deleteValutazioneById(this.idValCorso, "corso")
          .subscribe((res) => this.scaricaDati());
        break;
    }
  }
}
