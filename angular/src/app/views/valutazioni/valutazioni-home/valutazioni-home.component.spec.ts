import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValutazioniHomeComponent } from './valutazioni-home.component';

describe('ValutazioniHomeComponent', () => {
  let component: ValutazioniHomeComponent;
  let fixture: ComponentFixture<ValutazioniHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValutazioniHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValutazioniHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
