import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {

  constructor() { }

  public docente: any = {
    "completezza": ["c1", "c2", "c3", "c4", "c5"],
    "chiarezza": ["e1", "e2", "e3", "e4", "e5"],
    "disponibilita": ["d1", "d2", "d3", "d4", "d5"]
  }

  public corso: any = {
    "crescita_professionale": ["cp1", "cp2", "cp3", "cp4", "cp5"],
    "lavoro_attuale": ["la1", "la2", "la3", "la4", "la5"],
    "argomenti": ["arg1", "arg2", "arg3", "arg4", "arg5"],
    "approfondimento": ["app1", "app2", "app3", "app4", "app5"],
    "durata": ["dur1", "dur2", "dur3", "dur4", "dur5"],
    "divisione_teoria_pratica": ["dtp1", "dtp2", "dtp3", "dtp4", "dtp5"],
    "esercitazioni": ["ese1", "ese2", "ese3", "ese4", "ese5"],
    "materiale_didattico": ["md1", "md2", "md3", "md4", "md5"],
  }
}
