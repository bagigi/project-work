import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValutazioniCorsiComponent } from './valutazioni-corsi.component';

describe('ValutazioniCorsiComponent', () => {
  let component: ValutazioniCorsiComponent;
  let fixture: ComponentFixture<ValutazioniCorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValutazioniCorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValutazioniCorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
