import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
  {
    name: "Menu",
    icon: "icon-menu",

    children: [
      {
        name: "Home",
        url: "/dashboard",
        icon: "icon-login",
      },
    ],
  },
  {
    name: "Docenti",
    icon: "icon-list",
    children: [
      {
        name: "Docenti",
        url: "/docenti",
        icon: "icon-people",
      },
      {
        name: "Docenti Interni",
        url: "/docenti/interni",
        icon: "icon-people",
      },
      {
        name: "Nuovo Docente",
        url: "/docenti/nuovo",
        icon: "icon-plus",
      },
    ],
  },
  {
    name: "Dipendenti",
    url: "/dipendenti",
    icon: "icon-people",
  },
  {
    name: "Corsi",
    icon: "icon-list",
    children: [
      {
        name: "Corsi",
        url: "/corsi/tutto",
        icon: "icon-layers",
      },
      {
        name: "Nuovo Corso",
        url: "/corsi/nuovo",
        icon: "icon-plus",
      },
    ],
  },
];
