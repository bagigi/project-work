import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Import Containers
import { DefaultLayoutComponent } from "./containers";

import { P404Component } from "./views/error/404.component";
import { P500Component } from "./views/error/500.component";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "dashboard",
    pathMatch: "full",
  },
  {
    path: "404",
    component: P404Component,
    data: {
      title: "Page 404",
    },
  },
  {
    path: "500",
    component: P500Component,
    data: {
      title: "Page 500",
    },
  },

  {
    path: "",
    component: DefaultLayoutComponent,
    data: {
      title: "Home",
    },
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("./views/dashboard/dashboard.module").then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: "docenti",
        loadChildren: () =>
          import("./views/docenti/docenti.module").then((m) => m.DocentiModule),
      },
      {
        path: "dipendenti",
        loadChildren: () =>
          import("./views/dipendenti/dipendenti.module").then(
            (m) => m.DipendentiModule
          ),
      },
      {
        path: "corsi",
        loadChildren: () =>
          import("./views/corsi/corsi.module").then((m) => m.CorsiModule),
      },
      {
        path: "valutazioni",
        loadChildren: () =>
          import("./views/valutazioni/valutazioni.module").then((m) => m.ValutazioniModule),
      },
    ],
  },
  { path: "**", component: P404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
