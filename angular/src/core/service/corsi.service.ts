import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CorsiService {
  public path: string = "api/corsi/";

  constructor(public api: ApiService) {}

  public getPages() {
    return this.api.get(this.path + "info/pages");
  }

  public getByPage(page: number) {
    return this.api.get(this.path + "page/" + page);
  }

  public newCorso(tipoDocente: any, idDocente: any, corso: any) {
    return this.api.post(this.path + tipoDocente + "/" + idDocente, corso);
  }

  public getByTipologia(tipologia: any) {
    return this.api.get(this.path + "/tipologia/" + tipologia);
  }

  public getDocenteId(id: any) {
    return this.api.get(this.path + "/info/docente/" + id);
  }

  public getTipoDocente(id: any) {
    return this.api.get(this.path + "/info/docente/tipo/" + id);
  }

  public getById(id: any) {
    return this.api.get(this.path + id);
  }

  public getArgomento(id: any) {
    return this.api.get(this.path + "info/argomento/" + id);
  }

  public getPagesCorsiInCorso(idAlunno: any) {
    return this.api.get(this.path + "info/page/alunno/" + idAlunno);
  }

  public getCorsiInCorso(idAlunno: any, page: any) {
    return this.api.get(this.path + "alunno/" + idAlunno + "/" + page);
  }

  public deleteById(id: any) {
    return this.api.deleteNoSlash(this.path, id);
  }

  public getRelazioni(id: any) {
    return this.api.get(this.path + "info/relazioni/" + id);
  }

  replace(item: any, id): Observable<any> {
    console.log("cor ser", item);
    return this.api.replace(this.path + id, item);
  }
}
