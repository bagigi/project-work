import { TestBed, async, inject } from '@angular/core/testing';
import { DocentiService } from './docenti.service';

describe('Service: Docenti', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocentiService]
    });
  });

  it('should ...', inject([DocentiService], (service: DocentiService) => {
    expect(service).toBeTruthy();
  }));
});