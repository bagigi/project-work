import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class DipendentiService {
  constructor(private api: ApiService) { }
  private readonly path = "api/dipendenti";

  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }
  getById(id: string) {
    return this.api.get(this.path + "/id/" + id);
  }
  getByMatricola(matricola: any) {
    return this.api.get(this.path + "/" + matricola);
  }
  add(item: any): Observable<any> {
    const obj = {
      nome: item.nome,
      cognome: item.cognome
    };
    return this.api.post(this.path + "/create", obj);
  }
  public deleteById(id: string): Observable<any> {
    return this.api.delete(this.path, id);
  }
  replace(item: any): Observable<any> {
    console.log("dip ser", item);
    return this.api.replace(this.path, item);
  }
  filter(key: string, value: any) {
    return this.api.filter(this.path, key, value);
  }
  getFilter(value: string) {
    return this.api.get(this.path + "/filter/" + value);
  }


  getByPage(page: number) {
    return this.api.get(this.path + "/page/" + page);
  }

  getPages() {
    return this.api.get(this.path + "/info/pages/all");
  }

  getFilterPages(percorso: String) {
    return this.api.get(this.path + "" + percorso);
  }

  getFilterList(percorso: String) {
    return this.api.get(this.path + "" + percorso);
  }
}