import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ValutazioniService {
  constructor(private api: ApiService) {}
  private readonly path = "api/valutazioni";

  public valutaUtente(item: any): Observable<any> {
    return this.api.post(this.path + "/utente", item);
  }

  public valutaCorso(item: any): Observable<any> {
    return this.api.post(this.path + "/corso", item);
  }

  public valutaDocente(item: any): Observable<any> {
    return this.api.post(this.path + "/docente", item);
  }

  public GetValutazioniCorso(item: any) {
    return this.api.get(this.path + "/corso/" + item);
  }

  public getPagesValutazioniByDocente(id: any, tipo: any) {
    return this.api.get(this.path + "/info/page/docente/" + tipo + "/" + id);
  }

  public getPagesValutazioniByDipendente(id: any) {
    return this.api.get(this.path + "/info/page/alunno/" + id);
  }

  public GetValutazioniDocente(item: any, tipoDocente: any, page: any) {
    return this.api.get(
      this.path + "/docente/" + tipoDocente + "/" + item + "/" + page
    );
  }

  public GetValutazioniDip(item: any, page: any) {
    return this.api.get(this.path + "/dipendente/" + item + "/" + page);
  }

  public getValutazioneDocenteByCorso(docente: any, corso: any) {
    return this.api.get(this.path + "/docente/" + docente + "/corso/" + corso);
  }

  public deleteValutazioneById(id: any, tipo: any) {
    return this.api.delete(this.path + "/" + tipo, id);
  }

  public getValutazioneDipendenteByCorso(dipendente: any, corso: any) {
    return this.api.get(
      this.path + "/dipendente/" + dipendente + "/corso/" + corso
    );
  }
}
