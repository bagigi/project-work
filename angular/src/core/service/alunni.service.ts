import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AlunniService {
  public path: string = "api/alunni/"

  constructor(public api: ApiService) { }

  public insertMultiple(idCorso: any, lista: any) {
    return this.api.post(this.path + idCorso, lista);
  }

  public getByCorsoId(idCorso: any) {
    return this.api.get(this.path + "corso/" + idCorso);
  }

  public deleteById(idDipendente: any, idCorso: any) {
    return this.api.delete(this.path + idDipendente + "/", idCorso);
  }
}
