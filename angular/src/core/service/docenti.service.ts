import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class DocentiService {
  constructor(private api: ApiService) {}
  private readonly path = "api/docenti";

  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }

  getById(id: string) {
    return this.api.get(this.path + "/" + id);
  }

  add(item: any): Observable<any> {
    return this.api.post(this.path, item);
  }

  public deleteById(id: string): Observable<any> {
    console.log("path: ", this.path);
    return this.api.delete(this.path, id);
  }

  replace(item: any, id): Observable<any> {
    console.log("doc ser", item);
    return this.api.replace(this.path + "/" + id, item);
  }

  filter(key: string, value: any) {
    return this.api.filter(this.path, key, value);
  }

  getByPage(page: number) {
    return this.api.get(this.path + "/page/" + page);
  }

  getPages() {
    return this.api.get(this.path + "/info/pages/all");
  }

  getFilterPages(percorso: String) {
    return this.api.get(this.path + "/" + percorso);
  }

  getFilterList(percorso: String) {
    return this.api.get(this.path + "/" + percorso);
  }

  getDocentiByRagioneSociale(value: String) {
    return this.api.get(this.path + "/filter/" + value);
  }

  /**
   * Docenti interni
   */

  getDocentiInterniPages() {
    return this.api.get(this.path + "/info/interno/pages/all");
  }

  getDocentiInterni(page: any) {
    return this.api.get(this.path + "/interno/page/" + page);
  }

  getDocentiInterniFilterPage(filtro: any, valore: any) {
    return this.api.get(
      this.path +
        "/info/interno/pages/filtro?filtro=" +
        filtro +
        "&valore=" +
        valore
    );
  }

  getDocentiByFilter(filtro: any, valore: any, page: any) {
    return this.api.get(
      this.path +
        "/interno/filtro?filtro=" +
        filtro +
        "&valore=" +
        valore +
        "&page=" +
        page
    );
  }

  getRelazioni(id: any, tipo: any) {
    return this.api.get(this.path + "/info/relazioni/" + tipo + "/" + id);
  }
}
