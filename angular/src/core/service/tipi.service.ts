import { Injectable } from "@angular/core"
import { ApiService } from "./api.service"

@Injectable({
  providedIn: "root",
})
export class TipiService {
  public path: string = "api/tipi"

  constructor(private api: ApiService) {}

  public getAll() {
    return this.api.get(this.path)
  }
}
